#include "MiniginPCH.h"
#include "UpdateComponent.h"

LittleEngine::UpdateComponent::UpdateComponent(std::function<void()> updateFunc)
	:m_UserUpdate{updateFunc}
{
}

void LittleEngine::UpdateComponent::Initialize()
{
}

void LittleEngine::UpdateComponent::Update()
{
	if(!m_IsDisabled)
		m_UserUpdate();
}

void LittleEngine::UpdateComponent::Render()
{
}

void LittleEngine::UpdateComponent::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::UpdateComponent::Enable()
{
	m_IsDisabled = false;
}
