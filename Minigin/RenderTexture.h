#pragma once
#include "BaseComponent.h"
namespace LittleEngine
{
	class Texture2D;
	class RenderTextureComp : public BaseComponent
	{
	public:
		RenderTextureComp(const std::string& fileName, float xPos = 0.f, float yPos = 0.f, float scale = 1.0f);
		~RenderTextureComp();
		void Initialize() override {};
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;
		RenderTextureComp(const RenderTextureComp&) = delete;
		RenderTextureComp(RenderTextureComp&&) = delete;
		RenderTextureComp& operator= (const RenderTextureComp&) = delete;
		RenderTextureComp& operator= (const RenderTextureComp&&) = delete;
	private:
		Texture2D* m_pTexture;
		float m_xPos;
		float m_yPos;
		float m_Scale;
	};
}

