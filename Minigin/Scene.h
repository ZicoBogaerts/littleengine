#pragma once
#include "SceneManager.h"
#include <functional>

namespace LittleEngine
{
	class GameObject;
	class Scene final
	{
		/*friend Scene& SceneManager::CreateScene(const std::string& name);
		friend void SceneManager::SetActiveScene(int index);
		friend void SceneManager::SetActiveScene(const std::string& name);*/
	public:
		void Add(GameObject* object, bool shouldInitialize = false);

		void SceneUpdate();
		void SceneRender() const;
		void SceneInitialize();
		void SceneActivated();
		void RemoveGameObject(GameObject* pObject);

		virtual ~Scene();
		explicit Scene(const std::string& name,std::function<void()> initFunc = std::function<void()>(), std::function<void()> updateFunc = std::function<void()>(), std::function<void()> renderFunc = std::function<void()>(), std::function<void()> sceneActivationFunc = std::function<void()>());
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;
		
		const std::string& GetName() const { return m_Name; }
	private: 
		std::string m_Name;
		std::vector <GameObject*> m_pObjects{};
		std::vector<GameObject*> m_pObjectsToBeDeleted;
		std::function<void()> m_UserUpdate;
		std::function<void()> m_UserInitialize;
		std::function<void()> m_UserRender;
		std::function<void()> m_UserSceneActivation;

	};

}
