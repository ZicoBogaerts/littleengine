#pragma once
#include <string>

namespace LittleEngine
{
	class Audio
	{
	public:
		virtual ~Audio() = default;
		virtual void AddSoundEffect(const std::string& path, int id) = 0;
		virtual void AddSoundStream(const std::string& path, int id) = 0;
		virtual void PlaySoundEffect(int soundId, int loops) = 0;
		virtual void PlaySoundStream(int streamId, bool IsRepeating) = 0;
		virtual void SetVolumeSoundEffect(int soundId, int volume) = 0;
		virtual int GetVolumeSoundEffect(int soundId) const = 0;
		virtual void PauseAllMusic() = 0;
		virtual void ResumeAllMusic() = 0;
		virtual int GetVolumeMusic() = 0;
		virtual void SetVolumeMusic(int volume) = 0;
	private:

	};
}
