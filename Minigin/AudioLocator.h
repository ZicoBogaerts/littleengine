#pragma once
#include "NullAudio.h"

namespace LittleEngine
{
	class AudioLocator
	{
	public:
		static void Initialize();
		static Audio& GetAudio() { return *m_pAudioService; }
		static void ProvideService(Audio* pService);
	private:
		static Audio* m_pAudioService;
		static NullAudio m_pDefaultNullAudio;
	};
}

