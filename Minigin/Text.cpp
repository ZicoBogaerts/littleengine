#include "MiniginPCH.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include "Text.h"
#include "Renderer.h"
#include "Font.h"
#include "Texture2D.h"

LittleEngine::Text::Text(const std::string& text,Font* pFont ,float xPos, float yPos)
    : BaseComponent()
    , m_Text{text}
	, m_pFont{pFont}
	, m_Color{ SDL_Color{255,255,255,255} }
	, m_NeedsUpdate{true}
	, m_pTexture{}
{
    m_Position.x = xPos;
    m_Position.y = yPos;
}

LittleEngine::Text::~Text()
{
	delete m_pTexture;
	m_pTexture = nullptr;
}



void LittleEngine::Text::SetText(const std::string& text)
{
	m_Text = text;
	m_NeedsUpdate = true;
}

void LittleEngine::Text::SetColor(const SDL_Color& color)
{
	m_Color = color;
}

void LittleEngine::Text::Update()
{
	if (m_NeedsUpdate && !m_IsDisabled)
	{
		const auto surf = TTF_RenderText_Blended(m_pFont->GetFont(), m_Text.c_str(), m_Color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		delete m_pTexture;
		m_pTexture = new Texture2D(texture);
		m_NeedsUpdate = false;
	}
}

void LittleEngine::Text::Render()
{
	if (m_pTexture != nullptr && !m_IsDisabled)
	{
		const auto pos = m_Position;
		Renderer::GetInstance().RenderTexture(*m_pTexture, pos.x, pos.y);
	}
}

void LittleEngine::Text::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::Text::Enable()
{
	m_IsDisabled = false;
}
