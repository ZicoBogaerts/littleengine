#include "MiniginPCH.h"
#include "BaseComponent.h"

LittleEngine::BaseComponent::BaseComponent()
	: m_Parent{nullptr}
	, m_IsDisabled{false}
{
}

LittleEngine::BaseComponent::~BaseComponent()
{
	m_Parent = nullptr;
}
