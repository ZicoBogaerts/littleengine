#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"
#include <algorithm>
#include <future>

LittleEngine::SceneManager::SceneManager()
	: m_IdActiveScene{0}
{

}

void LittleEngine::SceneManager::SetActiveScene(int index)
{
	if (index > -1 && size_t(index) < m_pScenes.size() && index != m_IdActiveScene)
	{
		m_IdActiveScene = index;
		m_pScenes[m_IdActiveScene]->SceneActivated();
	}
}

void LittleEngine::SceneManager::SetActiveScene(const std::string& name)
{
	auto it = std::find_if(m_pScenes.begin(), m_pScenes.end(), [&name](Scene* pScene) {return pScene->GetName() == name; });
	if (it != m_pScenes.end())
	{
		int newIndex = int(it - m_pScenes.begin());
		if (newIndex != m_IdActiveScene)
		{
			m_IdActiveScene = newIndex;
			m_pScenes[m_IdActiveScene]->SceneActivated();
		}
	}
}

void LittleEngine::SceneManager::Initialize()
{
	std::vector<std::future<void>> futures;
	for (auto pScene : m_pScenes)
	{
		pScene->SceneInitialize();
		//futures.push_back(std::async(std::launch::async, &LittleEngine::Scene::SceneInitialize, pScene));
	}
	for (auto& future : futures)
	{
		future.get();
	}
}

void LittleEngine::SceneManager::Update()
{
	m_pScenes[m_IdActiveScene]->SceneUpdate();
}

void LittleEngine::SceneManager::Render()
{
	m_pScenes[m_IdActiveScene]->SceneRender();
}

LittleEngine::Scene& LittleEngine::SceneManager::CreateScene(const std::string& name)
{
	const auto scene = new Scene(name);
	m_pScenes.push_back(scene);
	return *scene;
}

void LittleEngine::SceneManager::AddScene(Scene* pScene)
{
	m_pScenes.push_back(pScene);
}

LittleEngine::Scene* LittleEngine::SceneManager::GetScene(int index) const
{
	if (index < 0 || size_t(index) < m_pScenes.size())
		return m_pScenes[index];
	return nullptr;
}

LittleEngine::SceneManager::~SceneManager()
{
	for (size_t i{ m_pScenes.size() - 1 }; i != -1; i--)
	{
		delete m_pScenes[i];
		m_pScenes[i] = nullptr;
	}
}
