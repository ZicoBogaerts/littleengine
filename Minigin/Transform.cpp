#include "MiniginPCH.h"
#include "Transform.h"

LittleEngine::Transform::Transform()
	: m_Position{0.f,0.f,0.f}
{
}

void LittleEngine::Transform::SetPosition(const float x, const float y, const float z)
{
	m_Position.x = x;
	m_Position.y = y;
	m_Position.z = z;
}

void LittleEngine::Transform::Update()
{
}

void LittleEngine::Transform::Render()
{
}

void LittleEngine::Transform::Disable()
{
}

void LittleEngine::Transform::Enable()
{
}
