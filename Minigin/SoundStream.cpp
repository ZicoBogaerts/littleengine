#include "MiniginPCH.h"
#include "SoundStream.h"

LittleEngine::SoundStream::SoundStream(const std::string& path)
	: m_pMixMusic{Mix_LoadMUS(path.c_str())}
{
}

LittleEngine::SoundStream::~SoundStream()
{
	Mix_FreeMusic(m_pMixMusic);
	m_pMixMusic = nullptr;
}

bool LittleEngine::SoundStream::IsLoaded() const
{
	return m_pMixMusic != nullptr;
}

bool LittleEngine::SoundStream::Play(bool repeat) const
{
	int result{ Mix_PlayMusic(m_pMixMusic, repeat ? -1 : 1) };
	return result == 0 ? true : false;
}
