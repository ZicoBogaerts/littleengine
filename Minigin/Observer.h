#pragma once
namespace LittleEngine
{
	class Observer
	{
	public:
		virtual ~Observer() = default;
		virtual void OnNotify(int event) = 0;
	private:
	};
}

