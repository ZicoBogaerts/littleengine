#pragma once

namespace LittleEngine
{
	struct Float2
	{
		float x, y;
		Float2()
			:x{0}
			, y{0}
		{}
		Float2(const Float2& vec)
			:x{vec.x}
			, y{vec.y}
		{}
	};

	struct Float3
	{
		float x, y, z;
		Float3()
			: x{0}
			, y{0}
			, z{0}
		{}
		Float3(float x, float y, float z)
			: x{x}
			, y{y}
			, z{z}
		{}
		void operator=(const Float3& other)
		{
			x = other.x;
			y = other.y;
			z = other.z;
		}
	};

	struct RectF
	{
		float x, y, width, height;
	};
}