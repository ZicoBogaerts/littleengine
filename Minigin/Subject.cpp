#include "MiniginPCH.h"
#include "Subject.h"
#include "Observer.h"

void LittleEngine::Subject::AddObserver(Observer* pObserver)
{
	m_pObservers.push_back(pObserver);
}

void LittleEngine::Subject::RemoveObserver(Observer* pObserver)
{
	m_pObservers.erase(std::find(m_pObservers.begin(), m_pObservers.end(), pObserver));
}

void LittleEngine::Subject::Notify(int event)
{
	for (Observer* pObserver : m_pObservers)
	{
		pObserver->OnNotify(event);
	}
}
