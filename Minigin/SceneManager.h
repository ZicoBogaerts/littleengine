#pragma once
#include "Singleton.h"
#include <string>
#include <vector>

namespace LittleEngine
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene& CreateScene(const std::string& name);
		void AddScene(Scene* pScene);
		Scene* GetActiveScene() { return m_pScenes[m_IdActiveScene]; };
		Scene* GetScene(int index) const;
		~SceneManager();

		void SetActiveScene(int index);
		void SetActiveScene(const std::string& name);
		void Initialize();
		void Update();
		void Render();
	private:
		friend class Singleton<SceneManager>;
		SceneManager();
		std::vector<Scene*> m_pScenes;
		int m_IdActiveScene;
	};
}
