#pragma once
#include <string>
#include <string>
#include <fstream>
#include <iostream>
#include "Structs.h"

namespace LittleEngine
{
	template <typename T>
	class BinaryReadAndWrite
	{
	public:
		static void ReadData(T& data, std::ifstream& ifstream);
		static void WriteData(const T& data, std::ofstream& ofstream);
	private:
	};


	template<typename T>
	inline void BinaryReadAndWrite<T>::ReadData(T& data, std::ifstream& ifstream)
	{
		if (ifstream.is_open() && std::is_pod<T>())
		{
			ifstream.read((char*)&data, sizeof(T));
		}
	}

	template<>
	inline void BinaryReadAndWrite<std::string>::ReadData(std::string& data, std::ifstream& ifstream)
	{
		if (ifstream.is_open())
		{
			size_t size{};
			ifstream.read((char*)&size, sizeof(size_t));
			char* temp = new char[size];
			ifstream.read(temp, size);
			data.append(temp, size);
			delete[] temp;
		}
	}

	template<>
	inline void BinaryReadAndWrite<Float3>::ReadData(Float3& data, std::ifstream& ifstream)
	{
		if (ifstream.is_open())
		{
			ifstream.read((char*)&data, sizeof(Float3));
		}
	}

	template <typename T>
	inline void BinaryReadAndWrite<T>::WriteData(const T& data, std::ofstream& ofstream)
	{
		if (ofstream.is_open() && std::is_pod<T>())
		{
			ofstream.write((const char*)&data, sizeof(T));
		}
	}


	template <>
	inline void BinaryReadAndWrite<std::string>::WriteData(const std::string& data, std::ofstream& ofstream)
	{
		if (ofstream.is_open())
		{
			size_t size = data.size();
			ofstream.write((const char*)&size, sizeof(size_t));
			ofstream.write(data.c_str(), size);
		}
	}

	template<>
	inline void BinaryReadAndWrite<Float3>::WriteData(const Float3& vertices, std::ofstream& ofstream)
	{
		if (ofstream.is_open())
		{
			ofstream.write((const char*)&vertices, sizeof(Float3));
		}
	}
}



