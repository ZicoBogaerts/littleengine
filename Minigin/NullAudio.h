#pragma once
#include "Audio.h"
namespace LittleEngine
{
	class NullAudio : public Audio
	{
	public:
		void AddSoundEffect(const std::string&, int) override {};
		void AddSoundStream(const std::string&, int) override {};
		void PlaySoundEffect(int, int) override {};
		void PlaySoundStream(int, bool) override {};
		void SetVolumeSoundEffect(int, int) {};
		int GetVolumeSoundEffect(int) const override { return -1; };
		void PauseAllMusic() override {};
		void ResumeAllMusic() override {};
		int GetVolumeMusic() override { return -1; };
		void SetVolumeMusic(int) override {};
	private:

	};
}

