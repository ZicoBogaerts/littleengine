#pragma once
#include "BaseComponent.h"
namespace LittleEngine
{
	class Text;
	class FPS : public BaseComponent
	{
	public:
		FPS(Text* pText);
		~FPS() = default;
		void Initialize() override {};
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;
		int GetFPS() { return m_FPS; };

		FPS(const FPS& other) = delete;
		FPS(FPS&& other) = delete;
		FPS& operator=(const FPS& other) = delete;
		FPS& operator=(FPS&& other) = delete;
	private:
		int m_FPS;
		int m_FPSCount;
		float m_FpsTimer;
		Text* m_pText;
	};
}

