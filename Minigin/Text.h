#pragma once
#include "BaseComponent.h"
#include <SDL.h>
#include <SDL_ttf.h>

namespace LittleEngine
{
	class Font;
	class Texture2D;
	class Text : public BaseComponent
	{
	public:
		Text(const std::string& text,Font* pFont ,float xPos, float yPos);
		~Text();
		void SetText(const std::string& text);
		void SetColor(const SDL_Color& color);
		void Initialize() override {};
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;
		std::string GetText() const { return m_Text; }

		Text(const Text& other) = delete;
		Text(Text&& other) = delete;
		Text& operator=(const Text& other) = delete;
		Text& operator=(Text&& other) = delete;
	private:
		std::string m_Text;
		LittleEngine::Float3 m_Position;
		Font* m_pFont;
		Texture2D* m_pTexture;
		SDL_Color m_Color;
		bool m_NeedsUpdate;
	};
}

