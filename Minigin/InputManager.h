#pragma once
#include "Singleton.h"
#include "Command.h"
#include <Windows.h>
#include <XInput.h>
#include <map>

namespace LittleEngine
{
	enum class ButtonState
	{
		pressed,
		released,
		hold
	};

	struct InputCommand
	{
		InputCommand()
			: id{ -1 }
			, pCommand{nullptr}
			, gamePadCode{0}
			, keyboardCode{-1}
			, buttonState{ButtonState::pressed}
			, isActive{false}
		{
		}

		InputCommand(int id, Command* pCommand,ButtonState buttonState = ButtonState::pressed ,int gamePadCode = 0,int keyboardCode = -1)
			:id{id}
			, pCommand{pCommand}
			, gamePadCode{gamePadCode}
			, keyboardCode{keyboardCode}
			, buttonState{buttonState}
			, isActive{true}
		{}
		int id;
		int gamePadCode;
		int keyboardCode;
		Command* pCommand;
		ButtonState buttonState;
		bool isActive;
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		InputManager(int numberOfControllers = 4);
		~InputManager();
		bool ProcessInput();
		void ChangeStatusInputCommand(int inputCommandId, bool IsActive);
		void AddInputAction(InputCommand inputAction);
		void SetMaxAmountControllers(int maxNumberOfControllers);
	private:
		std::vector<XINPUT_STATE> m_CurrentStates;
		std::vector<XINPUT_STATE> m_PreviousStates;
		std::vector<short> m_CurrentKeyboardState;
		std::vector<short> m_PreviousKeyboardState;
		std::map<int, InputCommand> m_Actions;
		int m_MaxNumberOfControllers;
	};

}
