#pragma once
#include "BaseComponent.h"
#include <functional>

namespace LittleEngine
{
    class UpdateComponent : public BaseComponent
    {
    public:
        //Recommend using a lambda to call your class function and passing the lambda
        UpdateComponent(std::function<void()> updateFunc);
        void Initialize() override;
        void Update() override;
        void Render() override;
        void Disable() override;
        void Enable() override;

        UpdateComponent(const UpdateComponent& other) = default;
        UpdateComponent(UpdateComponent&& other) = default;
        UpdateComponent& operator=(const UpdateComponent& other) = default;
        UpdateComponent& operator=(UpdateComponent&& other) = default;
    private:
        std::function<void()> m_UserUpdate;
    };
}

