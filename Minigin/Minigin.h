#pragma once
struct SDL_Window;

namespace LittleEngine
{
	class Minigin
	{
	public:
		void Initialize();
		void Cleanup();
		void Run();
	private:
		static const float SecondsPerFrame; //0.016 for 60 fps, 0.033 for 30 fps
		SDL_Window* m_Window{};
	};
}