#pragma once
#include <vector>
namespace LittleEngine
{
	class Observer;
	class Subject
	{
	public:
		void AddObserver(Observer* pObserver);
		void RemoveObserver(Observer* pObserver);
		void Notify(int event);
	private:
		std::vector<Observer*> m_pObservers;
	};
}

