#include "MiniginPCH.h"
#include "Time.h"
using namespace LittleEngine;

void LittleEngine::Time::Update(float deltaTime)
{
	m_DeltaTime = deltaTime;
}

LittleEngine::Time::Time()
	: m_DeltaTime{}
	, m_FixedTimeStep{0.f}
{
}
