#include "MiniginPCH.h"
#include "ResourceManager.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <algorithm>
#include <future>
#include <fstream>

#include "Renderer.h"
#include "Texture2D.h"
#include "Font.h"

LittleEngine::ResourceManager::~ResourceManager()
{
	for (auto it{ m_pTextures.begin() }; it != m_pTextures.end(); it++)
	{
		SDL_DestroyTexture(it->second);
		it->second = nullptr;
	}
	m_pTextures.erase(m_pTextures.begin(), m_pTextures.end());
	for (auto it{ m_pFonts.begin() }; it != m_pFonts.end(); it++)
	{
		delete it->second.second;
		it->second.second = nullptr;
	}
	m_pFonts.erase(m_pFonts.begin(), m_pFonts.end());
}

void LittleEngine::ResourceManager::Init(const std::string& dataPath)
{
	m_DataPath = dataPath;

	// load support for png and jpg, this takes a while!
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) 
	{
		throw std::runtime_error(std::string("Failed to load support for png's: ") + SDL_GetError());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) 
	{
		throw std::runtime_error(std::string("Failed to load support for jpg's: ") + SDL_GetError());
	}

	if (TTF_Init() != 0) 
	{
		throw std::runtime_error(std::string("Failed to load support for fonts: ") + SDL_GetError());
	}
}

SDL_Texture* LittleEngine::ResourceManager::GetTexture(const std::string& file)
{
	auto it = m_pTextures.find(file);
	if (it == m_pTextures.end())
	{
		throw std::runtime_error("Asking for texture that is not loaded: " + file);
	}
	return m_pTextures[file];
}

LittleEngine::Font* LittleEngine::ResourceManager::GetFont(const std::string& file)
{
	auto it = m_pFonts.find(file);
	if (it == m_pFonts.end())
	{
		throw std::runtime_error("Asking for font that is not loaded: " + file);
	}
	return m_pFonts[file].second;
}


void LittleEngine::ResourceManager::LoadResources()
{
	std::vector<std::future<bool>> futureTextures;
	std::vector<std::future<bool>> futureFonts;

	std::ifstream in{ m_DataPath + "Resources.txt" };
	if (!in)
	{
		throw std::runtime_error("Resources text file not found");
	}
	bool areImagesLoaded{ false };
	while (!in.eof())
	{
		std::string line;
		std::getline(in, line);
		if (line.empty())
		{
			areImagesLoaded = true;
			continue;
		}
		if (areImagesLoaded)
		{
			size_t pos = line.find(',');
			std::string fileName{ line.substr(0,pos) };
			std::string sizeString{ line.substr(pos + 1) };
			unsigned int size = std::stoi(sizeString);
			CreateFontResource(fileName, size);
		}
		else
		{
			CreateTexture(line);
		}
	}
	in.close();
}


bool LittleEngine::ResourceManager::CreateTexture(std::string file)
{
	const auto fullPath = m_DataPath + file;
	auto texture = IMG_LoadTexture(Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
	if (texture == nullptr)
	{
		throw std::runtime_error(std::string("Failed to load texture: ") + SDL_GetError());
	}
	m_pTextures[file] = texture;
	return true;
}

bool LittleEngine::ResourceManager::CreateFontResource(std::string file,unsigned int size)
{
	const auto fullPath = m_DataPath + file;
	Font* pNewFont = new Font(fullPath, size);
	m_pFonts[file].second = pNewFont;
	return true;
}

