#include "MiniginPCH.h"
#include "SoundManager.h"
#include "SoundEffect.h"
#include "SoundStream.h"
#include <mutex>

static std::mutex m_SoundEffectMutex;
static std::mutex m_SoundStreamMutex;

LittleEngine::SoundManager::~SoundManager()
{
	for (auto it = m_pSoundEffects.begin(); it != m_pSoundEffects.end(); it++)
	{
		delete it->second;
		it->second = nullptr;
	}
	m_pSoundEffects.erase(m_pSoundEffects.begin(), m_pSoundEffects.end());
	for (auto it = m_pSoundStreams.begin(); it != m_pSoundStreams.end(); it++)
	{
		delete it->second;
		it->second = nullptr;
	}
	m_pSoundStreams.erase(m_pSoundStreams.begin(), m_pSoundStreams.end());
}

void LittleEngine::SoundManager::AddSoundEffect(const std::string& path,int id)
{
	std::lock_guard<std::mutex> lock(m_SoundEffectMutex);
	SoundEffect* soundEffect = new SoundEffect{ path };
	m_pSoundEffects[id] = soundEffect;
}

void LittleEngine::SoundManager::AddSoundStream(const std::string& path, int id)
{
	std::lock_guard<std::mutex> lock(m_SoundStreamMutex);
	SoundStream* soundStream = new SoundStream{ path };
	m_pSoundStreams[id] = soundStream;
}

void LittleEngine::SoundManager::PlaySoundEffect(int soundId,int loops)
{
	auto it = m_pSoundEffects.find(soundId);
	if (it == m_pSoundEffects.end())
		return;
	m_pSoundEffects[soundId]->Play(loops);
}

void LittleEngine::SoundManager::PlaySoundStream(int streamId, bool IsRepeating)
{
	auto it = m_pSoundStreams.find(streamId);
	if (it == m_pSoundStreams.end())
		return;
	m_pSoundStreams[streamId]->Play(IsRepeating);
}

void LittleEngine::SoundManager::SetVolumeSoundEffect(int soundId, int volume)
{
	auto it = m_pSoundEffects.find(soundId);
	if (it == m_pSoundEffects.end())
		return;
	m_pSoundEffects[soundId]->SetVolume(volume);
}

int LittleEngine::SoundManager::GetVolumeSoundEffect(int soundId) const
{
	auto it = m_pSoundEffects.find(soundId);
	if (it == m_pSoundEffects.end())
		return -1;
	return m_pSoundEffects.at(soundId)->GetVolume();
}

void LittleEngine::SoundManager::PauseAllMusic()
{
	Mix_PauseMusic();
}

void LittleEngine::SoundManager::ResumeAllMusic()
{
	Mix_ResumeMusic();
}

int LittleEngine::SoundManager::GetVolumeMusic()
{
	return Mix_VolumeMusic(-1);
}

void LittleEngine::SoundManager::SetVolumeMusic(int volume)
{
	Mix_VolumeMusic(volume);
}
