#pragma once
#include "Singleton.h"

class b2World;
class b2Body;
struct b2BodyDef;
namespace LittleEngine
{
	class PhysicsManager : public Singleton<PhysicsManager>
	{
	public:
		PhysicsManager();
		~PhysicsManager();
		void Update();
		b2World* GetPhysicsWorld() const { return m_pWorld; }
		b2Body* AddBody(const b2BodyDef* bodyDef);
	private:
		b2World* m_pWorld;
		float m_TimeStep;

	};
}


