#pragma once
#include "Singleton.h"

namespace LittleEngine
{
	class Minigin;
	class Time final : public Singleton<Time>
	{
	public:
		float GetDeltaTime() const { return m_DeltaTime; };
		void Update(float deltaTime);
		float GetFixedTimeStep() const { return m_FixedTimeStep; }
		void SetFixedTimeStep(float timeStep) { m_FixedTimeStep = timeStep; }
	private:
		friend class Singleton<Time>;
		Time();
		float m_DeltaTime;
		float m_FixedTimeStep;
	};
}

