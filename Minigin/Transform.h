#pragma once
#include "BaseComponent.h"
#include "Structs.h"

namespace LittleEngine
{
	class Transform final : public BaseComponent
	{
	public:
		Transform();
		const Float3& GetPosition() const { return m_Position; }
		void SetPosition(float x, float y, float z);
		void Initialize() override {};
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;

		Transform(const Transform& other) = default;
		Transform(Transform&& other) = default;
		Transform& operator=(const Transform& other) = default;
		Transform& operator=(Transform&& other) = default;
	private:
		Float3 m_Position;
	};
}

