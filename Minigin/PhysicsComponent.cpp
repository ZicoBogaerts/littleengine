#include "MiniginPCH.h"
#include "PhysicsComponent.h"
#include "PhysicsManager.h"
#include "Renderer.h"
#include "GameObject.h"
#include "Utils.h"


float LittleEngine::PhysicsComponent::m_PixelsPerMeter = 32.f;

void LittleEngine::PhysicsComponent::SetPixelsPerMeter(float pixelsPerMeter)
{
	m_PixelsPerMeter = pixelsPerMeter;
}

LittleEngine::PhysicsComponent::PhysicsComponent(const boxDefinition& boxDef, bool atRuntime)
	: m_BoxDef{boxDef}
	, m_pBody{nullptr}
	, m_IsReEnabled{false}
	, m_ShouldRemoveFixture{false}
	, m_FixtureIndex{0}
{
	if (atRuntime)
	{
		AddFixture(boxDef);
		m_pBody->SetUserData(boxDef.userData);
	}
}

LittleEngine::PhysicsComponent::~PhysicsComponent()
{
	b2World* pWorld = PhysicsManager::GetInstance().GetPhysicsWorld();
	if (pWorld != nullptr)
	{
	   pWorld->DestroyBody(m_pBody);
	}
}

void LittleEngine::PhysicsComponent::Initialize()
{
	AddFixture(m_BoxDef);
}

void LittleEngine::PhysicsComponent::Update()
{
	if (m_IsReEnabled)
	{
		m_pBody->SetEnabled(true);
		m_IsReEnabled = false;
	}
	if (m_ShouldRemoveFixture)
	{
		RemoveFixture(m_FixtureIndex);
		m_ShouldRemoveFixture = false;
	}
	if (!m_IsDisabled)
	{
		m_BoxDef.position = m_pBody->GetPosition();
		m_BoxDef.position.Set(m_BoxDef.position.x * m_PixelsPerMeter, m_BoxDef.position.y * m_PixelsPerMeter);
		GetParent()->SetPosition(m_BoxDef.position.x - m_BoxDef.width / 2.f - m_BoxDef.drawOffset.x, m_BoxDef.position.y - m_BoxDef.height / 2.f - m_BoxDef.drawOffset.y);
		return;
	}
	m_pBody->SetEnabled(false);
}

void LittleEngine::PhysicsComponent::Render()
{
	if (!m_IsDisabled && m_BoxDef.drawDebug)
	{
#ifdef _DEBUG
		for (b2Fixture* fix{ m_pBody->GetFixtureList() }; fix != nullptr; fix = fix->GetNext())
		{
			b2Shape::Type shapeType = fix->GetType();
			switch (shapeType)
			{
			case b2Shape::e_circle:
			{	
				b2CircleShape* pCircle = static_cast<b2CircleShape*>(fix->GetShape());
				b2Transform transform = m_pBody->GetTransform();
				b2Vec2 circlePos = pCircle->m_p;
				b2Mat33 transf{ {transform.q.c,transform.q.s,0},{-transform.q.s,transform.q.c,0},{transform.p.x,transform.p.y,1} };
				circlePos = b2Mul22(transf, circlePos);
				circlePos.x += transform.p.x;
				circlePos.y += transform.p.y;
				
				Renderer::GetInstance().RenderCircle(int(circlePos.x * m_PixelsPerMeter), int(circlePos.y * m_PixelsPerMeter), int(pCircle->m_radius * m_PixelsPerMeter));
			}
				break;
			case b2Shape::e_polygon:
			{
				b2PolygonShape* pShape = static_cast<b2PolygonShape*>(fix->GetShape());
				b2Transform transform = m_pBody->GetTransform();
				b2Mat33 transMat{ {transform.q.c,transform.q.s,0},{-transform.q.s,transform.q.c,0},{transform.p.x,transform.p.y,1} };
				b2Vec2 vertexPos[4];
				for (int i{ 0 }; i < 4; i++)
				{
					vertexPos[i] = pShape->m_vertices[i];
					vertexPos[i] = b2Mul22(transMat, vertexPos[i]);
					vertexPos[i].x += transform.p.x;
					vertexPos[i].y += transform.p.y;
				}
				int topLeftX = int(vertexPos[0].x * m_PixelsPerMeter);
				int topLeftY = int(vertexPos[0].y * m_PixelsPerMeter);
				int width = int(vertexPos[2].x * m_PixelsPerMeter - topLeftX);
				int height = int(vertexPos[2].y * m_PixelsPerMeter - topLeftY);
				Renderer::GetInstance().RenderRect(topLeftX, topLeftY, width, height);
			}
				break;
			default:
				break;
			}
		}

#endif
	}
}

void LittleEngine::PhysicsComponent::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::PhysicsComponent::Enable()
{
	m_IsDisabled = false;
	m_IsReEnabled = true;
}

void LittleEngine::PhysicsComponent::SetFilterData(const b2Filter& filterData,  int fixtureIndex)
{
	if (m_pBody)
	{
		b2Fixture* fix = m_pBody->GetFixtureList();
		int index{ 0 };
		while (index < fixtureIndex)
		{
			fix = fix->GetNext();
			index++;
		}
		if(fix != nullptr)
			fix->SetFilterData(filterData);
	}
}

void LittleEngine::PhysicsComponent::SetGravityScale(float scale)
{
	if (m_pBody)
	{
		m_pBody->SetGravityScale(scale);
	}
}

void LittleEngine::PhysicsComponent::ApplyForce(const b2Vec2& force)
{
	if (m_pBody)
	{
		m_pBody->ApplyForceToCenter(force, true);
	}
}

void LittleEngine::PhysicsComponent::SetVelocity(const b2Vec2& vel)
{
	if (m_pBody)
	{
		m_pBody->SetLinearVelocity(vel);
	}
}

void LittleEngine::PhysicsComponent::SetRotation(float angle)
{
	m_pBody->SetTransform(m_pBody->GetPosition(), angle);
}

void LittleEngine::PhysicsComponent::AddFixture(const boxDefinition& boxDef)
{
	if (m_pBody == nullptr)
	{
		b2BodyDef bodyDef;
		bodyDef.type = m_BoxDef.type;
		bodyDef.position.Set((m_BoxDef.position.x + m_BoxDef.width / 2.f) / m_PixelsPerMeter, (m_BoxDef.position.y + m_BoxDef.height / 2.f) / m_PixelsPerMeter);
		bodyDef.gravityScale = m_BoxDef.gravityScale;
		bodyDef.linearDamping = m_BoxDef.linearDamping;
		bodyDef.fixedRotation = m_BoxDef.fixedRotation;
		m_pBody = PhysicsManager::GetInstance().AddBody(&bodyDef);
		m_pBody->SetUserData(GetParent());
	}
	switch (boxDef.type)
	{
	case b2BodyType::b2_staticBody:
	{
		b2PolygonShape box;
		box.SetAsBox((boxDef.width / m_PixelsPerMeter) / 2.f, (boxDef.height / m_PixelsPerMeter) / 2.f, { boxDef.offset.x / m_PixelsPerMeter, boxDef.offset.y / m_PixelsPerMeter }, 0);
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &box;
		fixtureDef.density = boxDef.density;
		fixtureDef.friction = boxDef.friction;
		fixtureDef.filter.categoryBits = boxDef.categoryBits;
		fixtureDef.filter.maskBits = boxDef.maskBits;
		fixtureDef.userData = boxDef.userData;
		fixtureDef.isSensor = boxDef.isSensor;
		m_pBody->CreateFixture(&fixtureDef);
	}
	break;
	case b2BodyType::b2_dynamicBody:
	{
		b2FixtureDef fixtureDef;
		b2PolygonShape dynamicBox;
		b2CircleShape circle;
		switch (boxDef.shapeType)
		{
		case b2Shape::e_polygon:
		{
			dynamicBox.SetAsBox((boxDef.width / m_PixelsPerMeter) / 2.f, (boxDef.height / m_PixelsPerMeter) / 2.f, { boxDef.offset.x / m_PixelsPerMeter, boxDef.offset.y / m_PixelsPerMeter }, 0);
			fixtureDef.shape = &dynamicBox;
		}
		break;
		case b2Shape::e_circle:
		{
			circle.m_radius = (boxDef.radius / m_PixelsPerMeter);
			fixtureDef.shape = &circle;
		}
		break;
		default:
			break;
		}
		fixtureDef.density = boxDef.density;
		fixtureDef.friction = boxDef.friction;
		fixtureDef.restitution = boxDef.restitution;
		fixtureDef.filter.categoryBits = boxDef.categoryBits;
		fixtureDef.filter.maskBits = boxDef.maskBits;
		fixtureDef.userData = boxDef.userData;
		fixtureDef.isSensor = boxDef.isSensor;

		m_pBody->CreateFixture(&fixtureDef);
	}
	break;
	case b2BodyType::b2_kinematicBody:
	{
		b2FixtureDef fixtureDef;
		b2PolygonShape kinematicBox;
		b2CircleShape kinematicCircle;
		switch (boxDef.shapeType)
		{
		case b2Shape::e_polygon:
		{
			kinematicBox.SetAsBox((boxDef.width / m_PixelsPerMeter) / 2.f, (boxDef.height / m_PixelsPerMeter) / 2.f, { boxDef.offset.x / m_PixelsPerMeter, boxDef.offset.y / m_PixelsPerMeter }, 0);
			fixtureDef.shape = &kinematicBox;
		}
		break;
		case b2Shape::e_circle:
		{
			kinematicCircle.m_radius = (boxDef.radius / m_PixelsPerMeter);
			kinematicCircle.m_p.Set(boxDef.offset.x / m_PixelsPerMeter, boxDef.offset.y / m_PixelsPerMeter);
			fixtureDef.shape = &kinematicCircle;
		}
		break;
		default:
			break;
		}
		fixtureDef.density = boxDef.density;
		fixtureDef.friction = boxDef.friction;
		fixtureDef.isSensor = boxDef.isSensor;
		fixtureDef.filter.categoryBits = boxDef.categoryBits;
		fixtureDef.filter.maskBits = boxDef.maskBits;
		fixtureDef.userData = boxDef.userData;


		m_pBody->CreateFixture(&fixtureDef);
	}
	break;
	}
}

void LittleEngine::PhysicsComponent::RemoveFixture(int index)
{
	if (m_pBody)
	{
		b2Fixture* fix = m_pBody->GetFixtureList();
		int currentIndex{ 0 };
		while (currentIndex < index)
		{
			if (fix == nullptr)
				return;
			fix = fix->GetNext();
			currentIndex++;
		}
		m_pBody->DestroyFixture(fix);
	}
}

void LittleEngine::PhysicsComponent::RemoveFixtureInNextUpdate(int index)
{
	m_FixtureIndex = index;
	m_ShouldRemoveFixture = true;
}

void LittleEngine::PhysicsComponent::SetBodyPosition(float xPos, float yPos)
{
	float x = (xPos + m_BoxDef.width / 2.f) / m_PixelsPerMeter;
	float y = (yPos + m_BoxDef.height / 2.f) / m_PixelsPerMeter;
	m_pBody->SetTransform({ x,y }, m_pBody->GetAngle());
}
