#include "MiniginPCH.h"
#include "Minigin.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "PhysicsManager.h"
#include "SoundManager.h"
#include "AudioLocator.h"
#include "GameObject.h"
#include "Scene.h"
#include "Time.h"
#include "RenderTexture.h"
#include <chrono>
#include <thread>
#include <SDL.h>
#include <SDL_mixer.h>

using namespace std;
using namespace std::chrono;

const float LittleEngine::Minigin::SecondsPerFrame = 1 / 144.f;

void LittleEngine::Minigin::Initialize()
{
	srand(unsigned int(time(nullptr)));

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	m_Window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,
		480,
		SDL_WINDOW_OPENGL
	);
	if (m_Window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		std::cerr << "Minigin::Initialize( ), error when calling Mix_OpenAudio: " << Mix_GetError() << std::endl;
		return;
	}

	Renderer::GetInstance().Init(m_Window);
}

void LittleEngine::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	Mix_Quit();
	SDL_Quit();
}

void LittleEngine::Minigin::Run()
{
	Initialize();
	SoundManager* soundManager = new SoundManager{};
	AudioLocator::Initialize();
	AudioLocator::ProvideService(soundManager);
	ResourceManager::GetInstance().LoadResources();
	

	{
		auto& physics = PhysicsManager::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& renderer = Renderer::GetInstance();
		auto& input = InputManager::GetInstance();
		auto& time = Time::GetInstance();
		time.SetFixedTimeStep(SecondsPerFrame);
		sceneManager.Initialize();


		bool doContinue = true;
		auto lastTime = std::chrono::high_resolution_clock::now();
		float timePassedPerFrame = 0.0f;
		while (doContinue)
		{
			const auto currentTime = std::chrono::high_resolution_clock::now();
			float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count();
			lastTime = currentTime;
			timePassedPerFrame += deltaTime;
			doContinue = input.ProcessInput();
			time.Update(deltaTime);
			while (timePassedPerFrame >= SecondsPerFrame)
			{
				physics.Update();
				sceneManager.Update();
				timePassedPerFrame -= SecondsPerFrame;
			}
			renderer.Render();
		}
	}
	delete soundManager;
	soundManager = nullptr;
	Cleanup();
}
