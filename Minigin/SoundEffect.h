#pragma once

struct Mix_Chunk;
#include <string>
namespace LittleEngine
{
	class SoundEffect
	{
	public:
		explicit SoundEffect(const std::string& path);
		~SoundEffect();
		SoundEffect(const SoundEffect& other) = delete;
		SoundEffect& operator=(const SoundEffect& rhs) = delete;
		SoundEffect(SoundEffect&& other) = delete;
		SoundEffect& operator=(SoundEffect&& rhs) = delete;

		bool IsLoaded() const;
		bool Play(int loops) const;
		void SetVolume(int value);
		int GetVolume() const;
	private:
		Mix_Chunk* m_pMixChunk;
	};
}

