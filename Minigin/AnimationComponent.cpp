#include "MiniginPCH.h"
#include "AnimationComponent.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "Texture2D.h"
#include "Time.h"
#include "Structs.h"
#include "GameObject.h"

LittleEngine::AnimationComponent::AnimationComponent(const std::string& fileName, int nrRows, int nrCols, int totalNrFrames, float secPerFrame, float scale, bool UseOneRowPerAnimation, bool autoPlay)
	: m_NrOfFrames{totalNrFrames}
	, m_Cols{nrCols}
	, m_Rows{nrRows}
	, m_ActFrame{0}
	, m_CurrentRow{0}
	, m_TimesPlayed{0}
	, m_TimesToBePlayed{-1}
	, m_FrameSec{secPerFrame}
	, m_AccuSec{0.f}
	, m_Scale{scale}
	, m_UsingOneRowPerAnimation{UseOneRowPerAnimation}
	, m_IsPlaying{autoPlay}
	, m_pTexture{new Texture2D{fileName}}
{
}

LittleEngine::AnimationComponent::~AnimationComponent()
{
	delete m_pTexture;
}

void LittleEngine::AnimationComponent::Initialize()
{
}

void LittleEngine::AnimationComponent::Update()
{
	if (!m_IsDisabled && m_IsPlaying)
	{
		m_AccuSec += Time::GetInstance().GetFixedTimeStep();
		if (m_AccuSec >= m_FrameSec)
		{
			m_ActFrame++;
			if (m_UsingOneRowPerAnimation)
			{
				if (m_ActFrame == m_Cols)
				{
					m_TimesPlayed++;
				}
				m_ActFrame %= m_Cols;
			}
			else
			{
				if (m_ActFrame == m_NrOfFrames)
				{
					m_TimesPlayed++;
				}
				m_ActFrame %= m_NrOfFrames;
			}
			if (m_TimesPlayed >= m_TimesToBePlayed && m_TimesToBePlayed > 0)
			{
				m_IsPlaying = false;
			}
			m_AccuSec -= m_FrameSec;
		}
	}
}

void LittleEngine::AnimationComponent::Render()
{
	if (m_IsDisabled)
	{
		return;
	}
	RectF srcRect{};
	if (m_UsingOneRowPerAnimation)
	{
		srcRect.width = float(m_pTexture->GetTextureWidth() / m_Cols);
		srcRect.height = float(m_pTexture->GetTextureHeight() / m_Rows);
		srcRect.y = ((m_ActFrame + (m_CurrentRow * m_Cols)) / m_Cols) * srcRect.height;
		srcRect.x = (m_ActFrame % m_Cols) * srcRect.width;
	}
	else
	{
		srcRect.width = float(m_pTexture->GetTextureWidth() / m_Cols);
		srcRect.height = float(m_pTexture->GetTextureHeight() / m_Rows);
		srcRect.y = (m_ActFrame / m_Cols) * srcRect.height;
		srcRect.x = (m_ActFrame % m_Cols) * srcRect.width;
	}

    LittleEngine::Float3 pos = GetParent()->GetTransform().GetPosition();
	RectF destRect{};
	destRect.x = pos.x;
	destRect.y = pos.y;
	destRect.width = srcRect.width * m_Scale;
	destRect.height = srcRect.height * m_Scale;

	Renderer::GetInstance().RenderTexture(*m_pTexture, srcRect, destRect);
}

void LittleEngine::AnimationComponent::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::AnimationComponent::Enable()
{
	m_IsDisabled = false;
}

void LittleEngine::AnimationComponent::Play(int times)
{
	m_TimesPlayed = 0;
	m_TimesToBePlayed = times;
	m_IsPlaying = true;
}

void LittleEngine::AnimationComponent::SetCurrentRow(int currentRow)
{
	m_CurrentRow = currentRow % m_Rows;
}

void LittleEngine::AnimationComponent::SetUsingSingleRow(bool UseSingleRow)
{
	m_UsingOneRowPerAnimation = UseSingleRow;
}

void LittleEngine::AnimationComponent::SetCurrentFrame(int frame)
{
	m_ActFrame = frame;
}
