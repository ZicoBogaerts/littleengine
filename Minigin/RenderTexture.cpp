#include "MiniginPCH.h"
#include  "RenderTexture.h"
#include "Texture2D.h"
#include "GameObject.h"
#include "Transform.h"

LittleEngine::RenderTextureComp::RenderTextureComp(const std::string& fileName, float xPos, float yPos, float scale)
	: m_pTexture{new Texture2D(fileName)}
	, m_xPos{xPos}
	, m_yPos{yPos}
	, m_Scale{scale}
{
}

LittleEngine::RenderTextureComp::~RenderTextureComp()
{
	delete m_pTexture;
	m_pTexture = nullptr;
}


void LittleEngine::RenderTextureComp::Update()
{
}

void LittleEngine::RenderTextureComp::Render()
{
	if (!m_IsDisabled)
	{
		LittleEngine::Float3 pos = GetParent()->GetTransform().GetPosition();
		m_pTexture->RenderTexture(pos.x + m_xPos, pos.y + m_yPos,m_Scale);
	}
}

void LittleEngine::RenderTextureComp::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::RenderTextureComp::Enable()
{
	m_IsDisabled = false;
}
