#pragma once
#include "BaseComponent.h"
#include <Box2D.h>

namespace LittleEngine
{
	class PhysicsComponent : public BaseComponent
	{
	public:
		struct boxDefinition
		{
			boxDefinition()
				: type{b2BodyType::b2_staticBody}
				, shapeType{b2Shape::Type::e_polygon}
				, position{b2Vec2{0.f,0.f}}
				, offset{b2Vec2{0.f,0.f}}
				, drawOffset{b2Vec2{0.f,0.f}}
				, width{0}
				, height{0}
				, radius{0}
				, categoryBits{1}
				, maskBits{65535}
				, friction{1.f}
				, restitution{0.f}
				, density{1.f}
				, gravityScale{1.f}
				, linearDamping{1.f}
				, userData{this}
				, isSensor{false}
				, fixedRotation{false}
				, drawDebug{false}
			{}
			b2BodyType type;
			b2Shape::Type shapeType;
			b2Vec2 position;
			//Offset does not apply to dynamic bodies
			b2Vec2 offset;
			//Use this to offset dynamic bodies
			b2Vec2 drawOffset;
			int width;
			int height;
			int radius;
			uint16 categoryBits;
			uint16 maskBits;
			float friction;
			float restitution;
			float density;
			float gravityScale;
			float linearDamping;
			void* userData;
			bool isSensor;
			bool fixedRotation;
			bool drawDebug;
		};

		static void SetPixelsPerMeter(float pixelsPerMeter);
		PhysicsComponent(const boxDefinition& boxDef,bool atRuntime = false);
		~PhysicsComponent();
		void Initialize() override;
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;
		
		int GetWidth() const { return m_BoxDef.width; }
		int GetHeight() const { return m_BoxDef.height; }
		void SetFilterData(const b2Filter& filter, int fixtureIndex);
		void SetGravityScale(float scale);
		void ApplyForce(const b2Vec2& force);
		void SetVelocity(const b2Vec2& vel);
		//set the rotation of box2d body, angle is in radians
		void SetRotation(float angle);
		//Adds fixture to already existing body
		void AddFixture(const boxDefinition& boxDef);
		// index based on order of adding the fixtures to this body
		// Can't be called during contact callbacks
		void RemoveFixture(int index);
		//Call this during contact callbacks
		void RemoveFixtureInNextUpdate(int index);

		//Ignores physicss
		void SetBodyPosition(float xPos, float yPos);

		PhysicsComponent(const PhysicsComponent& other) = delete;
		PhysicsComponent(PhysicsComponent&& other) = delete;
		PhysicsComponent& operator=(const PhysicsComponent& other) = delete;
		PhysicsComponent& operator=(PhysicsComponent&& other) = delete;
	private:
		boxDefinition m_BoxDef;
		b2Body* m_pBody;
		static float m_PixelsPerMeter;
		bool m_IsReEnabled;
		bool m_ShouldRemoveFixture;
		int m_FixtureIndex;

	};
}

