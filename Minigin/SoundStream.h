#pragma once
#include <SDL_mixer.h>
#include <string>
namespace LittleEngine
{
	class SoundStream
	{
	public:
		explicit SoundStream(const std::string& path);
		~SoundStream();
		SoundStream(const SoundStream& other) = delete;
		SoundStream& operator=(const SoundStream& rhs) = delete;
		SoundStream(SoundStream&& other) = delete;
		SoundStream& operator=(SoundStream&& other) = delete;

		bool IsLoaded() const;
		bool Play(bool repeat) const;
	private:
		Mix_Music* m_pMixMusic;
	};
}

