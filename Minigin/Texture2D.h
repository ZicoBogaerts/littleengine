#pragma once
#include "BaseComponent.h"
struct SDL_Texture;
namespace LittleEngine
{
	/**
	 * Simple RAII wrapper for an SDL_Texture
	 */
	class Texture2D
	{
	public:
		SDL_Texture* GetSDLTexture() const;
		explicit Texture2D(const std::string& fileName);
		explicit Texture2D(SDL_Texture* texture);
		~Texture2D();
		void RenderTexture(float xPos, float yPos);
		void RenderTexture(float xPos, float yPos,float scale);
		void SetSDLTexture(SDL_Texture* texture) { m_Texture = texture; };
		int GetTextureWidth() const;
		int GetTextureHeight() const;

		Texture2D(const Texture2D &) = delete;
		Texture2D(Texture2D &&) = delete;
		Texture2D & operator= (const Texture2D &) = delete;
		Texture2D & operator= (const Texture2D &&) = delete;
	private:
		SDL_Texture* m_Texture;
		int m_TextureWidth;
		int m_TextureHeight;
	};
}
