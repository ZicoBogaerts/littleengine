#pragma once
#include "Audio.h"
#include <map>

namespace LittleEngine
{
	class SoundEffect;
	class SoundStream;
	class SoundManager : public Audio
	{
	public:
		~SoundManager();
		void AddSoundEffect(const std::string& path, int id) override;
		void AddSoundStream(const std::string& path, int id) override;
		void PlaySoundEffect(int soundId, int loops) override;
		void PlaySoundStream(int streamId, bool IsRepeating) override;
		void SetVolumeSoundEffect(int soundId, int volume) override;
		int GetVolumeSoundEffect(int soundId) const override;
		void PauseAllMusic() override;
		void ResumeAllMusic() override;
		int GetVolumeMusic() override;
		void SetVolumeMusic(int volume) override;
	private:
		std::map<int, SoundEffect*> m_pSoundEffects;
		std::map<int, SoundStream*> m_pSoundStreams;
	};
}

