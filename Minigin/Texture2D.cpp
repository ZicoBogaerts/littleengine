#include "MiniginPCH.h"
#include "Texture2D.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>

LittleEngine::Texture2D::~Texture2D()
{
}

void LittleEngine::Texture2D::RenderTexture(float xPos, float yPos)
{
	Renderer::GetInstance().RenderTexture(*this, xPos, yPos);
}

void LittleEngine::Texture2D::RenderTexture(float xPos, float yPos, float scale)
{
	RectF srcRect{};
	srcRect.x = 0;
	srcRect.y = 0;
	srcRect.width = float(m_TextureWidth);
	srcRect.height = float(m_TextureHeight);

	RectF dstRect{};
	dstRect.x = xPos;
	dstRect.y = yPos;
	dstRect.width = m_TextureWidth * scale;
	dstRect.height = m_TextureHeight * scale;

	Renderer::GetInstance().RenderTexture(*this, srcRect, dstRect);
}

int LittleEngine::Texture2D::GetTextureWidth() const
{
	return m_TextureWidth;
}

int LittleEngine::Texture2D::GetTextureHeight() const
{
	return m_TextureHeight;
}

SDL_Texture* LittleEngine::Texture2D::GetSDLTexture() const
{
	return m_Texture;
}

LittleEngine::Texture2D::Texture2D(const std::string& fileName)
	: m_TextureHeight{}
	, m_TextureWidth{}
{
	m_Texture = ResourceManager::GetInstance().GetTexture(fileName);
	int querySucces = SDL_QueryTexture(m_Texture, nullptr, nullptr, &m_TextureWidth, &m_TextureHeight);
	if (querySucces != 0)
	{
		std::cout << "Error quering texture" << std::endl;
	}
}

LittleEngine::Texture2D::Texture2D(SDL_Texture* texture)
	: m_TextureHeight{0}
	, m_TextureWidth{0}
{
	m_Texture = texture;
	int querySucces = SDL_QueryTexture(m_Texture, nullptr, nullptr, &m_TextureWidth, &m_TextureHeight);
	if (querySucces != 0)
	{
		std::cout << "Error quering texture" << std::endl;
	}
}
