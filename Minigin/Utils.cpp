#include "MiniginPCH.h"
#include "Utils.h"

float LittleEngine::DegreesToRadians(float degrees)
{
    return degrees * PI / 180.f;
}

float LittleEngine::RadiansToDegrees(float rad)
{
    return rad * 180.f / PI;
}
