#pragma once

#define WINDOW_WIDTH 640.f
#define WINDOW_HEIGHT 480.f
#define PI 3.141593f
namespace LittleEngine
{
	float DegreesToRadians(float degrees);
	float RadiansToDegrees(float rad);
}

