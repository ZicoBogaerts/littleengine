#pragma once
#include "Structs.h"
namespace LittleEngine
{
	class GameObject;
	class BaseComponent
	{
	public:
		GameObject* GetParent() { return m_Parent; };
		void SetParent(GameObject* parent) { m_Parent = parent; };
		BaseComponent();
		virtual ~BaseComponent();

		virtual void Initialize() = 0;
		virtual void Update() = 0;
		virtual void Render() = 0;
		virtual void Disable() = 0;
		virtual void Enable() = 0;
	protected:
		bool m_IsDisabled;
	private:
		GameObject* m_Parent;
	};
}

