#include "MiniginPCH.h"
#include "SoundEffect.h"
#include <SDL_mixer.h>

LittleEngine::SoundEffect::SoundEffect(const std::string& path)
	: m_pMixChunk{Mix_LoadWAV(path.c_str())}
{
	if (m_pMixChunk == nullptr)
	{
		std::string errorMsg = "SoundEffect: Failed to load " + path + ",\nSDL_mixer Error: " + Mix_GetError();
		std::cerr << errorMsg;
	}
}

LittleEngine::SoundEffect::~SoundEffect()
{
	Mix_FreeChunk(m_pMixChunk);
	m_pMixChunk = nullptr;
}

bool LittleEngine::SoundEffect::IsLoaded() const
{
	return m_pMixChunk != nullptr;
}

bool LittleEngine::SoundEffect::Play(int loops) const
{
	int channel{ Mix_PlayChannel(-1,m_pMixChunk,loops) };
	return channel == -1 ? false : true;
}

void LittleEngine::SoundEffect::SetVolume(int value)
{
	Mix_VolumeChunk(m_pMixChunk, value);
}

int LittleEngine::SoundEffect::GetVolume() const
{
	return Mix_VolumeChunk(m_pMixChunk,-1);
}
