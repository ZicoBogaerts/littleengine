#include "MiniginPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2D.h"

void LittleEngine::Renderer::Init(SDL_Window * window)
{
	//m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (m_Renderer == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void LittleEngine::Renderer::Render() const
{
	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void LittleEngine::Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void LittleEngine::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void LittleEngine::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y, const float width, const float height) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	dst.w = static_cast<int>(width);
	dst.h = static_cast<int>(height);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void LittleEngine::Renderer::RenderTexture(const Texture2D& texture, const RectF& srcRect, const RectF& dstRect) const
{
	SDL_Rect src;
	src.x = static_cast<int>(srcRect.x);
	src.y = static_cast<int>(srcRect.y);
	src.w = static_cast<int>(srcRect.width);
	src.h = static_cast<int>(srcRect.height);

	SDL_Rect dst;
	dst.x = static_cast<int>(dstRect.x);
	dst.y = static_cast<int>(dstRect.y);
	dst.w = static_cast<int>(dstRect.width);
	dst.h = static_cast<int>(dstRect.height);

	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), &src, &dst);
}

void LittleEngine::Renderer::RenderCircle(int centreX, int centreY, int radius)
{
	const int32_t diameter = (radius * 2);

	int32_t x = (radius - 1);
	int32_t y = 0;
	int32_t tx = 1;
	int32_t ty = 1;
	int32_t error = (tx - diameter);

	while (x >= y)
	{
		SDL_SetRenderDrawColor(m_Renderer, 255, 0, 0, 255);
		//  Each of the following renders an octant of the circle
		SDL_RenderDrawPoint(m_Renderer, centreX + x, centreY - y);
		SDL_RenderDrawPoint(m_Renderer, centreX + x, centreY + y);
		SDL_RenderDrawPoint(m_Renderer, centreX - x, centreY - y);
		SDL_RenderDrawPoint(m_Renderer, centreX - x, centreY + y);
		SDL_RenderDrawPoint(m_Renderer, centreX + y, centreY - x);
		SDL_RenderDrawPoint(m_Renderer, centreX + y, centreY + x);
		SDL_RenderDrawPoint(m_Renderer, centreX - y, centreY - x);
		SDL_RenderDrawPoint(m_Renderer, centreX - y, centreY + x);
		SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 255);

		if (error <= 0)
		{
			++y;
			error += ty;
			ty += 2;
		}

		if (error > 0)
		{
			--x;
			tx += 2;
			error += (tx - diameter);
		}
	}
}

void LittleEngine::Renderer::RenderRect(int topLeftX, int topLeftY, int width, int height)
{
	SDL_Rect rect;
	rect.x = topLeftX;
	rect.y = topLeftY;
	rect.w = width;
	rect.h = height;
	SDL_SetRenderDrawColor(m_Renderer, 255, 0, 0, 255);
	SDL_RenderDrawRect(m_Renderer, &rect);
	SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 255);
}
