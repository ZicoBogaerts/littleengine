#include "MiniginPCH.h"
#include "AudioLocator.h"

LittleEngine::Audio* LittleEngine::AudioLocator::m_pAudioService = nullptr;
LittleEngine::NullAudio LittleEngine::AudioLocator::m_pDefaultNullAudio = NullAudio{};

void LittleEngine::AudioLocator::Initialize()
{
	m_pAudioService = &m_pDefaultNullAudio;
}

void LittleEngine::AudioLocator::ProvideService(Audio* pService)
{
	if (pService == nullptr)
	{
		m_pAudioService = &m_pDefaultNullAudio;
	}
	else
	{
		m_pAudioService = pService;
	}
}
