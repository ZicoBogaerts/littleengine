#include "MiniginPCH.h"
#include "FPS.h"
#include "Time.h"
#include "GameObject.h"
#include "Text.h"


LittleEngine::FPS::FPS(Text* pText)
	: m_FPS{0}
	, m_FPSCount{0}
	, m_FpsTimer{0.f}
	, m_pText{pText}
{
}

void LittleEngine::FPS::Update()
{
	if (!m_IsDisabled)
	{
		m_FpsTimer += LittleEngine::Time::GetInstance().GetFixedTimeStep();
		m_FPSCount++;
		if (m_FpsTimer >= 1.0f)
		{
			if (m_FPSCount != m_FPS)
			{
				m_FPS = m_FPSCount;
				if (m_pText != nullptr)
				{
					m_pText->SetText("FPS: " + std::to_string(m_FPS));
				}
			}
			m_FpsTimer -= 1.0f;
			m_FPSCount = 0;
		}
	}
}

void LittleEngine::FPS::Render()
{

}

void LittleEngine::FPS::Disable()
{
	m_IsDisabled = true;
}

void LittleEngine::FPS::Enable()
{
	m_IsDisabled = false;
}
