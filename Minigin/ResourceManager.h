#pragma once
#include "Singleton.h"
#include <unordered_map>
struct SDL_Texture;
namespace LittleEngine
{
	class Font;
	class ResourceManager final : public Singleton<ResourceManager>
	{
	public:
		~ResourceManager();
		void Init(const std::string& data);
		SDL_Texture* GetTexture(const std::string& file);
		Font* GetFont(const std::string& file);
		void LoadResources();
	private:
		friend class Singleton<ResourceManager>;
		ResourceManager() = default;
		std::string m_DataPath;
		std::unordered_map<std::string,std::pair<int,Font*>> m_pFonts;
		std::unordered_map<std::string,SDL_Texture*> m_pTextures;

		bool CreateTexture(std::string file);
		bool CreateFontResource(std::string file, unsigned int size);
	};
}
