#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"

using namespace LittleEngine;

void LittleEngine::Scene::SceneInitialize()
{
	if(m_UserInitialize)
		m_UserInitialize();

	for (auto& object : m_pObjects)
	{
		object->Initialize();
	}
}

void LittleEngine::Scene::SceneActivated()
{
	if (m_UserSceneActivation)
		m_UserSceneActivation();
}

void LittleEngine::Scene::RemoveGameObject(GameObject* pObject)
{
	m_pObjectsToBeDeleted.push_back(pObject);
}

Scene::Scene(const std::string& name, std::function<void()> initFunc, std::function<void()> updateFunc, std::function<void()> renderFunc, std::function<void()> sceneActivationFunc)
	: m_Name(name)
	, m_UserInitialize{initFunc}
	, m_UserUpdate{updateFunc}
	, m_UserRender{renderFunc}
	, m_UserSceneActivation{sceneActivationFunc}
{}

Scene::~Scene()
{
	for (size_t i{ m_pObjects.size() - 1 }; i != -1; i--)
	{
		delete m_pObjects[i];
		m_pObjects[i] = nullptr;
	}
}

void LittleEngine::Scene::Add(GameObject* object, bool shouldInitialize)
{
	m_pObjects.push_back(object);
	if(shouldInitialize)
		object->Initialize();
}

void Scene::SceneUpdate()
{
	if(m_UserUpdate)
		m_UserUpdate();
	int size = int(m_pObjects.size());
	for (int i{ 0 }; i < size; i++)
	{
		m_pObjects[i]->Update();
	}

	for (auto& object : m_pObjectsToBeDeleted)
	{
		auto it = std::find(m_pObjects.begin(), m_pObjects.end(), object);
		if (it != m_pObjects.end())
		{
			delete *it;
			*it = nullptr;
			m_pObjects.erase(it);
		}
	}
	m_pObjectsToBeDeleted.clear();
}

void Scene::SceneRender() const
{
	if(m_UserRender)
		m_UserRender();

	for (const auto& object : m_pObjects)
	{
		object->Render();
	}
}

