#include "MiniginPCH.h"
#include "GameObject.h"
#include "BaseComponent.h"
#include "ResourceManager.h"
#include "Renderer.h"

LittleEngine::GameObject::~GameObject()
{
	for (size_t i = 0; i < m_Components.size(); ++i)
	{
		if (m_Components[i]->GetParent() == this)
		{
			delete m_Components[i];
			m_Components[i] = nullptr;
		}
	}
	m_Components.clear();
}

void LittleEngine::GameObject::Initialize()
{
	for (BaseComponent* comp : m_Components)
	{
		comp->Initialize();
	}
}

void LittleEngine::GameObject::Update()
{
	for (BaseComponent* comp : m_Components)
	{
		comp->Update();
	}
}

void LittleEngine::GameObject::Render() const
{
	for (BaseComponent* comp : m_Components)
	{
		comp->Render();
	}
}


void LittleEngine::GameObject::SetPosition(float x, float y)
{
	m_Transform.SetPosition(x, y, 0.0f);
}

void LittleEngine::GameObject::SetTag(const std::string& tag)
{
	m_Tag = tag;
}

void LittleEngine::GameObject::AddComponent(LittleEngine::BaseComponent* comp)
{
	if (comp->GetParent() == nullptr)
	{
		comp->SetParent(this);
	}
	m_Components.push_back(comp);
}

void LittleEngine::GameObject::Disable()
{
	for (BaseComponent* comp : m_Components)
	{
		comp->Disable();
	}
}

void LittleEngine::GameObject::Enable()
{
	for (BaseComponent* comp : m_Components)
	{
		comp->Enable();
	}
}
