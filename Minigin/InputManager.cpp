#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>
#include <mutex>

static std::mutex m_ActionsMutex;

LittleEngine::InputManager::InputManager(int maxNumberOfControllers)
	: m_MaxNumberOfControllers{maxNumberOfControllers}
{
	m_CurrentStates = std::vector<XINPUT_STATE>(maxNumberOfControllers);
	m_PreviousStates = std::vector<XINPUT_STATE>(maxNumberOfControllers);
	m_CurrentKeyboardState = std::vector<short>(256);
	m_PreviousKeyboardState	= std::vector<short>(256);
}

LittleEngine::InputManager::~InputManager()
{
	for (auto inputAction : m_Actions)
	{
		delete inputAction.second.pCommand;
		inputAction.second.pCommand = nullptr;
	}
}

bool LittleEngine::InputManager::ProcessInput()
{
	for (size_t i{ 0 }; i < size_t(m_MaxNumberOfControllers); i++)
	{
		m_PreviousStates[i] = m_CurrentStates[i];
		ZeroMemory(&m_CurrentStates[i], sizeof(XINPUT_STATE));
		XInputGetState(DWORD(i), &m_CurrentStates[i]);
	}

	m_PreviousKeyboardState = m_CurrentKeyboardState;
	for (int i{ 0 }; i < 256; i++)
	{
		m_CurrentKeyboardState[i] = GetAsyncKeyState(i);
	}

	for (auto it = m_Actions.begin(); it != m_Actions.end(); it++)
	{
		InputCommand currCommand = it->second;
		if (!currCommand.isActive)
			continue;
		switch (currCommand.buttonState)
		{
		case ButtonState::pressed:
			//GamePad
			if (currCommand.gamePadCode > 0)
			{
				for (int i{ 0 }; i < m_MaxNumberOfControllers; i++)
				{
					if ((m_CurrentStates[i].Gamepad.wButtons & currCommand.gamePadCode) && !(m_PreviousStates[i].Gamepad.wButtons & currCommand.gamePadCode))
					{
						currCommand.pCommand->Execute();
					}
				}
			}

			//Keyboard
			if (currCommand.keyboardCode >= 0)
			{
  				if (m_CurrentKeyboardState[currCommand.keyboardCode] != 0 && m_PreviousKeyboardState[currCommand.keyboardCode] == 0)
				{
					currCommand.pCommand->Execute();
				}
			}
			break;
		case ButtonState::released:
			//GamePad
			if (currCommand.gamePadCode > 0)
			{
				for (int i{ 0 }; i < m_MaxNumberOfControllers; i++)
				{
					if (!(m_CurrentStates[i].Gamepad.wButtons & currCommand.gamePadCode) && (m_PreviousStates[i].Gamepad.wButtons & currCommand.gamePadCode))
					{
						currCommand.pCommand->Execute();
					}
				}
			}

			//Keyboard
			if (currCommand.keyboardCode >= 0)
			{
				if (m_CurrentKeyboardState[currCommand.keyboardCode] == 0 && m_PreviousKeyboardState[currCommand.keyboardCode] != 0)
				{
					currCommand.pCommand->Execute();
				}
			}
			break;
		case ButtonState::hold:
			//GamePad
			if (currCommand.gamePadCode > 0)
			{
				for (int i{ 0 }; i < m_MaxNumberOfControllers; i++)
				{
					if ((m_CurrentStates[i].Gamepad.wButtons & currCommand.gamePadCode) && (m_PreviousStates[i].Gamepad.wButtons & currCommand.gamePadCode))
					{
						currCommand.pCommand->Execute();
					}
				}
			}
			//Keyboard
			if (currCommand.keyboardCode >= 0)
			{
				if (m_CurrentKeyboardState[currCommand.keyboardCode] != 0 && m_PreviousKeyboardState[currCommand.keyboardCode] != 0)
				{
					currCommand.pCommand->Execute();
				}
			}
			break;
		}
	}

	SDL_Event e;
	while (SDL_PollEvent(&e)) 
	{
		if (e.type == SDL_QUIT) 
		{
			return false;
		}
	}

	return true;
}


void LittleEngine::InputManager::ChangeStatusInputCommand(int inputCommandId, bool IsActive)
{
	auto it = m_Actions.find(inputCommandId);
	if (it != m_Actions.end())
		return;

	m_Actions[inputCommandId].isActive = IsActive;
}

void LittleEngine::InputManager::AddInputAction(InputCommand inputAction)
{
	auto it = m_Actions.find(inputAction.id);
	if (it != m_Actions.end())
		return;

	std::lock_guard<std::mutex> lock(m_ActionsMutex);
	m_Actions[inputAction.id] = inputAction;
}

void LittleEngine::InputManager::SetMaxAmountControllers(int maxNumberOfControllers)
{
	if (maxNumberOfControllers > 0)
	{
		m_MaxNumberOfControllers = maxNumberOfControllers;
		m_CurrentStates = std::vector<XINPUT_STATE>(maxNumberOfControllers);
		m_PreviousStates = std::vector<XINPUT_STATE>(maxNumberOfControllers);
	}
}

