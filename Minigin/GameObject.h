#pragma once
#include "Transform.h"
#include <vector>
#include <string>

namespace LittleEngine
{
	class BaseComponent;
	class Texture2D;
	class GameObject final
	{
	public:
		void Initialize();
		void Update();
		void Render() const;

		//Gets overwritten by physics if it was added
		void SetPosition(float x, float y);
		void SetTag(const std::string& tag);
		void AddComponent(BaseComponent* comp);
		void Disable();
		void Enable();

		template <class CompType>
		CompType* GetComponent() const
		{
			for (auto comp : m_Components)
			{
				if (CompType* component = dynamic_cast<CompType*>(comp))
					return component;
			}
			return nullptr;
		}

		Transform GetTransform() const { return m_Transform; };
		std::string GetTag() const { return m_Tag; }
		GameObject() = default;
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	private:
		Transform m_Transform;
		std::vector<BaseComponent*> m_Components;
		std::string m_Tag;
	};
}
