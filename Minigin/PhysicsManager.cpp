#include "MiniginPCH.h"
#include "PhysicsManager.h"
#include <Box2D.h>
#include <mutex>

static std::mutex m_WorldMutex;

LittleEngine::PhysicsManager::PhysicsManager()
	: m_pWorld{}
	, m_TimeStep{1.f/ 60.f}
{
	b2Vec2 gravity{ 0.0f,9.81f };
	m_pWorld = new b2World{ gravity };
	
}

LittleEngine::PhysicsManager::~PhysicsManager()
{
	delete m_pWorld;
	m_pWorld = nullptr;
}

void LittleEngine::PhysicsManager::Update()
{
	int32 velocityIterations = 8;
	int32 positionIterations = 3;
	m_pWorld->Step(m_TimeStep, velocityIterations, positionIterations);
}

b2Body* LittleEngine::PhysicsManager::AddBody(const b2BodyDef* bodyDef)
{
	std::lock_guard<std::mutex> lock(m_WorldMutex);
	return m_pWorld->CreateBody(bodyDef);
}
