#pragma once
#include "BaseComponent.h"
#include <string>
namespace LittleEngine
{
	class Texture2D;
	class AnimationComponent final : public BaseComponent
	{
	public:
		AnimationComponent(const std::string& fileName, int nrRows, int nrCols, int totalNrFrames,float secPerFrame, float scale = 1.f, bool UseOneRowPerAnimation = false, bool autoPlay = true );
		~AnimationComponent();

		void Initialize() override;
		void Update() override;
		void Render() override;
		void Disable() override;
		void Enable() override;

		bool IsPlaying() const { return m_IsPlaying; }
		//to play on loop pass 0
		void Play(int times);
		void SetCurrentRow(int currentRow);
		void SetUsingSingleRow(bool UseSingleRow);
		void SetCurrentFrame(int frame);

		AnimationComponent(const AnimationComponent& other) = delete;
		AnimationComponent(AnimationComponent&& other) = delete;
		AnimationComponent& operator=(const AnimationComponent& other) = delete;
		AnimationComponent& operator=(AnimationComponent&& other) = delete;
	private:
		int m_NrOfFrames;
		int m_Cols;
		int m_Rows;
		int m_ActFrame;
		int m_CurrentRow;
		int m_TimesPlayed;
		int m_TimesToBePlayed;
		float m_FrameSec;
		float m_AccuSec;
		float m_Scale;
		bool m_UsingOneRowPerAnimation;
		bool m_IsPlaying;
		Texture2D* m_pTexture;
	};
}

