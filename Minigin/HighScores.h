#pragma once
#include "Singleton.h"
#include <vector>

namespace LittleEngine
{
	class Text;
};

class HighScores : public LittleEngine::Singleton<HighScores>
{
public:
	HighScores();
private:
	std::vector<LittleEngine::Text*> m_pHighScoreTexts;
	std::vector<int> m_HighScores;
};

