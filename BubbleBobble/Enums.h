#pragma once
enum class objectCategories
{
	Boundary = 0x0001,
	Digger = 0x0002,
	MoneyBag = 0x0004,
	Background = 0x0008,
	Enemy = 0x0010,
};

enum class inputCommands
{
	moveRight = 0,
	moveLeft = 1,
	moveUp = 2,
	moveDown = 3,
	shootFireBall = 4,
	startGame = 5
};

enum class Scenes
{
	StartScene = 0,
	MainScene = 1
};

enum class Events
{
	emeraldPickedUp = 0,
	goldPickedUp = 1,
	enemyKilled = 2,
	diggerKilled = 3,
	emeraldPickedUpHobbin = 4
};

enum class Sounds
{
	backgroundMusic = 0
};