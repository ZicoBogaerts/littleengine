#include "MainScene.h"
#include "InputManager.h"
#include "JumpCommand.h"
#include "MoveRightCommand.h"
#include "ShootCommand.h"
#include "Digger.h"
#include "Background.h"
#include "PhysicsContactListener.h"
#include "PhysicsManager.h"
#include "Score.h"
#include "EnemyManager.h"
#include "Enums.h"
#include "Scene.h"
#include "SceneManager.h"
#include "HighScores.h"
#include "Subject.h"
#include "AudioLocator.h"

#include <iostream>
#include <fstream>
#include <sstream>


MainScene::~MainScene()
{
	delete m_pContactListener;
	delete m_pDigger;
	delete m_pEnemyManager;
	delete m_pBackground;
	delete m_pScore;
}

MainScene::MainScene(const std::string& name)
	: m_pContactListener{}
	, m_pDigger{}
	, m_pEnemyManager{}
	, m_pBackground{}
	, m_pScore{}
	, m_CurrentLevel{0}
{
	auto init = [this]() {this->Initialize(); };
	auto update = [this]() {this->Update(); };
	auto render = [this]() {this->Render(); };
	auto sceneActivation = [this]() {this->SceneActivated(); };
	LittleEngine::Scene* pScene = new LittleEngine::Scene(name, init, update, render, sceneActivation);
	LittleEngine::SceneManager::GetInstance().AddScene(pScene);
}

void MainScene::NextLevel()
{
	m_CurrentLevel++;
	if (m_CurrentLevel > 7)
	{
		LittleEngine::SceneManager::GetInstance().SetActiveScene(int(Scenes::StartScene));
		HighScores::GetInstance().AddHighScore();
		return;
	}
	m_pEnemyManager->NextLevel();
	m_pBackground->AssembleLevel(m_CurrentLevel, m_pScore);
	m_pDigger->SetBackToSpawnPos();
}

void MainScene::Initialize()
{
	LoadLevels();
	using namespace LittleEngine;

	m_pDigger = new Digger();
	m_pEnemyManager = new EnemyManager(m_pDigger,this);
	m_pScore = new Score(this);
	HighScores::GetInstance().SetScoreReference(m_pScore);
	MoveCommand* pMoveRight = new MoveCommand(m_pDigger,ControlableObject::MoveState::Right);
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::moveRight), pMoveRight, LittleEngine::ButtonState::hold, XINPUT_GAMEPAD_DPAD_RIGHT, VK_RIGHT));

	MoveCommand* pMoveLeft = new MoveCommand(m_pDigger, ControlableObject::MoveState::Left);
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::moveLeft), pMoveLeft, LittleEngine::ButtonState::hold, XINPUT_GAMEPAD_DPAD_LEFT, VK_LEFT));

	MoveCommand* pMoveUp = new MoveCommand(m_pDigger, ControlableObject::MoveState::Up);
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::moveUp), pMoveUp, LittleEngine::ButtonState::hold, XINPUT_GAMEPAD_DPAD_UP, VK_UP));

	MoveCommand* pMoveDown = new MoveCommand(m_pDigger, ControlableObject::MoveState::Down);
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::moveDown), pMoveDown, LittleEngine::ButtonState::hold, XINPUT_GAMEPAD_DPAD_DOWN, VK_DOWN));

	ShootCommand* pShoot = new ShootCommand(m_pDigger);
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::shootFireBall), pShoot, LittleEngine::ButtonState::pressed, XINPUT_GAMEPAD_A, VK_SPACE));

	m_pContactListener = new PhysicsContactListener{};
	PhysicsManager::GetInstance().GetPhysicsWorld()->SetContactListener(m_pContactListener);
	m_pContactListener->GetSubject()->AddObserver(m_pScore);
	m_pContactListener->GetSubject()->AddObserver(m_pEnemyManager);

	AudioLocator::GetAudio().AddSoundStream("../Resources/Sounds/PopCorn.mp3",int(Sounds::backgroundMusic));
	AudioLocator::GetAudio().SetVolumeMusic(2);
}

void MainScene::Update()
{
}

void MainScene::Render() const
{
}

void MainScene::SceneActivated()
{
	HighScores::GetInstance().Disable();
	m_pDigger->Reset();
	m_pEnemyManager->Reset();
	m_pScore->Reset();
	m_CurrentLevel = 0;
	m_pBackground->AssembleLevel(m_CurrentLevel, m_pScore);
	LittleEngine::AudioLocator::GetAudio().PlaySoundStream(int(Sounds::backgroundMusic), true);
	LittleEngine::AudioLocator::GetAudio().ResumeAllMusic();
}

void MainScene::LoadLevels()
{
	using namespace LittleEngine;
	std::ifstream in{ "../Resources/LevelData.txt" };
	if (!in)
	{
		throw std::runtime_error("Level data text file not found");
	}
	std::vector<std::string> levelData;
	levelData.push_back("");
	int currentLevel{ 0 };
	while (!in.eof())
	{
		std::string line;
		std::getline(in, line);
		if (line.empty())
		{
			currentLevel++;
			levelData.push_back("");
			continue;
		}
		levelData[currentLevel].append(line);
	}
	m_pBackground = new Background{ levelData };
}
