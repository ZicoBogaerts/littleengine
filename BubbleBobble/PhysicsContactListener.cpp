#include "PhysicsContactListener.h"
#include "GameObject.h"
#include "MoneyBag.h"
#include "SceneManager.h"
#include "Scene.h"
#include "Digger.h"
#include "Enemy.h"
#include "Subject.h"
#include "Enums.h"

PhysicsContactListener::PhysicsContactListener()
	: m_pSubject{new LittleEngine::Subject{}}
{
}

PhysicsContactListener::~PhysicsContactListener()
{
	delete m_pSubject;
	m_pSubject = nullptr;
}

void PhysicsContactListener::BeginContact(b2Contact* contact)
{
	using namespace LittleEngine;
	b2Fixture* fixA = contact->GetFixtureA();
	b2Fixture* fixB = contact->GetFixtureB();
	GameObject* objA = static_cast<GameObject*>( fixA->GetBody()->GetUserData());
	GameObject* objB = static_cast<GameObject*>( fixB->GetBody()->GetUserData());

	bool isADigger = objA->GetTag() == "Digger";
	bool isBDigger = objB->GetTag() == "Digger";
	bool isABackground = objA->GetTag() == "BackGround";
	bool isBBackground = objB->GetTag() == "BackGround";
	bool isABoundary = objA->GetTag() == "Boundary";
	bool isBBoundary = objB->GetTag() == "Boundary";
	bool isAHobbin = objA->GetTag() == "Hobbin";
	bool isBHobbin = objB->GetTag() == "Hobbin";
	bool isANobbin = objA->GetTag() == "Nobbin";
	bool isBNobbin = objB->GetTag() == "Nobbin";
	bool isAMoneyBag = objA->GetTag() == "MoneyBag";
	bool isBMoneyBag = objB->GetTag() == "MoneyBag";
	bool isAGold = objA->GetTag() == "Gold";
	bool isBGold = objB->GetTag() == "Gold";
	bool isAFireBall = objA->GetTag() == "FireBall";
	bool isBFireBall = objB->GetTag() == "FireBall";
	bool isAEmerald = objA->GetTag() == "Emerald";
	bool isBEmerald = objB->GetTag() == "Emerald";

	//Check for digging part of digger
	if (fixA->IsSensor() && isADigger && isBBackground)
	{
		objB->Disable();
		return;
	}
	if (fixB->IsSensor() && isBDigger && isABackground)
	{
		objA->Disable();
		return;
	}

	//Check for digging part of hobbin
	if (fixA->IsSensor() && isAHobbin && isBBackground)
	{
		objB->Disable();
		return;
	}
	if (fixB->IsSensor() && isBHobbin && isABackground)
	{
		objA->Disable();
		return;
	}

	if (isAEmerald && isBDigger && fixB->IsSensor())
	{
		m_pSubject->Notify(int(Events::emeraldPickedUp));
		objA->Disable();
		return;
	}
	if (isADigger && fixA->IsSensor() && isBEmerald)
	{
		m_pSubject->Notify(int(Events::emeraldPickedUp));
		objB->Disable();
		return;
	}
	//Check for freeing bags
	if ((isABackground || isABoundary) && isBMoneyBag && fixB->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixB->GetUserData());
		pMoneyBag->AddBackgroundTouch();
		return;
	}
	if ((isBBackground || isBBoundary) && isAMoneyBag && fixA->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixA->GetUserData());
		pMoneyBag->AddBackgroundTouch();
		return;
	}
	//Check for killing digger through falling bags
	if ((isADigger || isAHobbin) && !fixA->IsSensor() && isBMoneyBag && !fixB->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixB->GetUserData());
		if (pMoneyBag->IsFalling() && isADigger)
		{
			Digger* pDigger = static_cast<Digger*>(fixA->GetUserData());
			pDigger->HitByFallingBag();
			m_pSubject->Notify(int(Events::diggerKilled));
			return;
		}
		pMoneyBag->SetBeingPushed(true);
		return;
	}
	if ((isBDigger || isBHobbin) && !fixB->IsSensor() && isAMoneyBag && !fixA->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixA->GetUserData());
		if (pMoneyBag->IsFalling() && isBDigger)
		{
			Digger* pDigger = static_cast<Digger*>(fixB->GetUserData());
			pDigger->HitByFallingBag();
			m_pSubject->Notify(int(Events::diggerKilled));
			return;
		}
		pMoneyBag->SetBeingPushed(true);
		return;
	}

	//Gold and digger
	if (isAGold && isBDigger && fixB->IsSensor())
	{
		m_pSubject->Notify(int(Events::goldPickedUp));
		objA->Disable();
		return;
	}
	if (isBGold && isADigger && fixA->IsSensor())
	{
		m_pSubject->Notify(int(Events::goldPickedUp));
		objB->Disable();
		return;
	}

	//Check for spawning rip animation and removing killed digger
	if (!fixA->IsSensor() && isADigger && (isBBackground || isBBoundary))
	{
		Digger* pDigger = static_cast<Digger*>(fixA->GetUserData());
		pDigger->SpawnDeathAnimation();
		return;
	}
	if (!fixB->IsSensor() && isBDigger && (isABackground || isABoundary))
	{
		Digger* pDigger = static_cast<Digger*>(fixB->GetUserData());
		pDigger->SpawnDeathAnimation();
		return;
	}

	//Check fireball
	if (fixA->IsSensor() && isAFireBall && (isBBackground || isBBoundary))
	{
		Digger* pDigger = static_cast<Digger*>(fixA->GetUserData());
		pDigger->ProjectileCollision();
		return;
	}
	if (fixB->IsSensor() && isBFireBall && (isABackground || isABoundary))
	{
		Digger* pDigger = static_cast<Digger*>(fixB->GetUserData());
		pDigger->ProjectileCollision();
		return;
	}
	//Check fireball and enemies
	if (fixA->IsSensor() && isAFireBall && !fixB->IsSensor() &&  (isBNobbin || isBHobbin))
	{
		Digger* pDigger = static_cast<Digger*>(fixA->GetUserData());
		pDigger->ProjectileCollision();
		Enemy* pEnemy = static_cast<Enemy*>(fixB->GetUserData());
		pEnemy->Disable();
		m_pSubject->Notify(int(Events::enemyKilled));
		return;
	}
	if (fixB->IsSensor() && isBFireBall && !fixA->IsSensor() && (isANobbin || isAHobbin))
	{
		Digger* pDigger = static_cast<Digger*>(fixB->GetUserData());
		pDigger->ProjectileCollision();
		Enemy* pEnemy = static_cast<Enemy*>(fixA->GetUserData());
		pEnemy->Disable();
		m_pSubject->Notify(int(Events::enemyKilled));
		return;
	}

	//Check for nobbin and background/boundary for movement
	if (fixA->IsSensor() && isANobbin && (isBBackground || isBBoundary))
	{
		int direction = int(fixA->GetDensity());
		Enemy* pEnemy = static_cast<Enemy*>(fixA->GetUserData());
		pEnemy->AddBackgroundTouch(direction);
		return;
	}
	if (fixB->IsSensor() && isBNobbin && (isABackground || isABoundary))
	{
		int direction = int(fixB->GetDensity());
		Enemy* pEnemy = static_cast<Enemy*>(fixB->GetUserData());
		pEnemy->AddBackgroundTouch(direction);
		return;
	}

	//Check for nobbin/hobbin and background/boundary when killed
	if (!fixA->IsSensor() && (isANobbin || isAHobbin) && (isBBackground || isBBoundary))
	{
		Enemy* pEnemy = static_cast<Enemy*>(fixA->GetUserData());
		if (pEnemy->GetHitByBag() && !pEnemy->IsDisabled())
		{
			pEnemy->Disable();
			m_pSubject->Notify(int(Events::enemyKilled));
		}
		return;
	}
	if (!fixB->IsSensor() && (isBNobbin || isBHobbin) && (isABackground || isABoundary))
	{
		Enemy* pEnemy = static_cast<Enemy*>(fixB->GetUserData());
		if (pEnemy->GetHitByBag() && !pEnemy->IsDisabled())
		{
			pEnemy->Disable();
			m_pSubject->Notify(int(Events::enemyKilled));
		}
		return;
	}

	//check for nobbin/hobbin and digger
	if (!fixA->IsSensor() && isADigger && !fixB->IsSensor() && (isBNobbin || isBHobbin))
	{
		Digger* pDigger = static_cast<Digger*>(fixA->GetUserData());
		m_pSubject->Notify(int(Events::diggerKilled));
		pDigger->HitByEnemy();
		return;
	}
	if (!fixB->IsSensor() && isBDigger && !fixA->IsSensor() && (isANobbin || isAHobbin))
	{
		Digger* pDigger = static_cast<Digger*>(fixB->GetUserData());
		pDigger->HitByEnemy();
		m_pSubject->Notify(int(Events::diggerKilled));
		return;
	}

	//check for nobbin/hobbin and bag
	if ((isANobbin || isAHobbin) && !fixA->IsSensor() && isBMoneyBag && !fixB->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixB->GetUserData());
		if (pMoneyBag->IsFalling())
		{
			Enemy* pEnemy = static_cast<Enemy*>(fixA->GetUserData());
			pEnemy->HitByBag();
			return;
		}
		return;
	}
	if ((isBNobbin || isBHobbin) && !fixB->IsSensor() && isAMoneyBag && !fixA->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixA->GetUserData());
		if (pMoneyBag->IsFalling())
		{
			Enemy* pEnemy = static_cast<Enemy*>(fixB->GetUserData());
			pEnemy->HitByBag();
			return;
		}
		return;
	}

	//Check for nobbing for removing bag/emerald
	if (isAHobbin && fixA->IsSensor() && (isBMoneyBag || isBEmerald))
	{
		objB->Disable();
		if(isBEmerald)
			m_pSubject->Notify(int(Events::emeraldPickedUpHobbin));
		return;
	}
	if (isBHobbin && fixB->IsSensor() && (isAMoneyBag || isAEmerald))
	{
		objA->Disable();
		if(isAEmerald)
			m_pSubject->Notify(int(Events::emeraldPickedUpHobbin));
		return;
	}
}

void PhysicsContactListener::EndContact(b2Contact* contact)
{
	using namespace LittleEngine;
	b2Fixture* fixA = contact->GetFixtureA();
	b2Fixture* fixB = contact->GetFixtureB();
	b2Body* bodyA = fixA->GetBody();
	b2Body* bodyB = fixB->GetBody();
	GameObject* objA = static_cast<GameObject*>(bodyA->GetUserData());
	GameObject* objB = static_cast<GameObject*>(bodyB->GetUserData());

	bool isADigger = objA->GetTag() == "Digger";
	bool isBDigger = objB->GetTag() == "Digger";
	bool isABackground = objA->GetTag() == "BackGround";
	bool isBBackground = objB->GetTag() == "BackGround";
	bool isABoundary = objA->GetTag() == "Boundary";
	bool isBBoundary = objB->GetTag() == "Boundary";
	bool isAHobbin = objA->GetTag() == "Hobbin";
	bool isBHobbin = objB->GetTag() == "Hobbin";
	bool isANobbin = objA->GetTag() == "Nobbin";
	bool isBNobbin = objB->GetTag() == "Nobbin";
	bool isAMoneyBag = objA->GetTag() == "MoneyBag";
	bool isBMoneyBag = objB->GetTag() == "MoneyBag";

	if ((isABackground || isABoundary) && isBMoneyBag && fixB->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>( fixB->GetUserData());
		pMoneyBag->SubstractBackgroundTouch();
		return;
	}
	if ((isBBackground || isBBoundary) && isAMoneyBag && fixA->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixA->GetUserData());
		pMoneyBag->SubstractBackgroundTouch();
		return;
	}
	if ((isADigger || isAHobbin) && !fixA->IsSensor() && isBMoneyBag && !fixB->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixB->GetUserData());
		pMoneyBag->SetBeingPushed(false);
		return;
	}
	if ((isBDigger || isBHobbin) && !fixB->IsSensor() && isAMoneyBag && !fixA->IsSensor())
	{
		MoneyBag* pMoneyBag = static_cast<MoneyBag*>(fixA->GetUserData());
		pMoneyBag->SetBeingPushed(false);
		return;
	}

	//Check for nobbin movement
	if (fixA->IsSensor() && isANobbin && (isBBackground || isBBoundary))
	{
		int direction = int(fixA->GetDensity());
		Enemy* pEnemy = static_cast<Enemy*>(fixA->GetUserData());
		pEnemy->SubstractBackgroundTouch(direction);
		return;
	}
	if (fixB->IsSensor() && isBNobbin && (isABackground || isABoundary))
	{
		int direction = int(fixB->GetDensity());
		Enemy* pEnemy = static_cast<Enemy*>(fixB->GetUserData());
		pEnemy->SubstractBackgroundTouch(direction);
		return;
	}
}

