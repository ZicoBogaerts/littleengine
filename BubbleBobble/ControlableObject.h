#pragma once
class ControlableObject
{
public:
	enum class MoveState
	{
		Down = 0,
		Left = 1,
		Right = 2,
		Up = 3
	};
	virtual ~ControlableObject() = default;
	virtual void SetVelocity(MoveState moveState) = 0;
private:

};

