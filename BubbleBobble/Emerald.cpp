#include "Emerald.h"
#include "GameObject.h"
#include "RenderTexture.h"
#include "Scene.h"
#include "SceneManager.h"
#include "PhysicsComponent.h"

Emerald::Emerald(float xPos, float yPos, int nodeWidth, int nodeHeight)
{
	using namespace LittleEngine;
	GameObject* emerald = new GameObject();
	emerald->AddComponent(new RenderTextureComp("CEMERALD.png"));
	emerald->SetTag("Emerald");
	PhysicsComponent::boxDefinition boxDef;
	boxDef.type = b2_kinematicBody;
	boxDef.offset = { 6.f,0.f };
	boxDef.position = { xPos,yPos};
	boxDef.height = nodeHeight / 2;
	boxDef.width = nodeWidth / 2 + 4;
	boxDef.isSensor = true;
	PhysicsComponent* pPhysicsComp = new PhysicsComponent(boxDef);
	emerald->AddComponent(pPhysicsComp);
	SceneManager::GetInstance().GetActiveScene()->Add(emerald);
	emerald->SetPosition(xPos, yPos);
}
