#include "StartGameCommand.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Enums.h"

void StartGameCommand::Execute()
{
	LittleEngine::SceneManager::GetInstance().SetActiveScene(int(Scenes::MainScene));
}
