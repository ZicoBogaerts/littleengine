#include "EnemyManager.h"
#include "Enemy.h"
#include "Time.h"
#include "GameObject.h"
#include "UpdateComponent.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Enums.h"
#include "MainScene.h"

EnemyManager::EnemyManager(Digger* pDigger, MainScene* pScene)
	: m_MaxEnemies{8}
	, m_CurrentMaxEnemies{5}
	, m_CurrentEnemiesSpawned{0}
	, m_EnemiesAlive{0}
	, m_TimeBetweenSpawns{5.f}
	, m_CurrentTimeBetweenSpawns{4.f}
	, m_TimeForRestart{5.f}
	, m_CurrentTimeRestart{0.f}
	, m_StopSpawning{false}
	, m_GoToNextLevel{false}
	, m_pScene{pScene}
{
	m_StartPos.x = 540.f;
	m_StartPos.y = 72.f;
	for (int i{}; i < m_MaxEnemies; i++)
	{
		m_pEnemies.push_back(new Enemy(m_StartPos.x, m_StartPos.y, pDigger));
		m_pEnemies[i]->Disable();
	}
	using namespace LittleEngine;
	GameObject* pObject = new GameObject();
	auto update = [this]() {this->Update(); };
	UpdateComponent* pUpdate = new UpdateComponent(update);
	pObject->AddComponent(pUpdate);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(pObject);
}

EnemyManager::~EnemyManager()
{
	for (auto enemy : m_pEnemies)
	{
		delete enemy;
		enemy = nullptr;
	}
	m_pEnemies.clear();
}

void EnemyManager::OnNotify(int event)
{
	switch (event)
	{
	case int(Events::enemyKilled) :
		EnemyDied();
		break;
	case int(Events::diggerKilled) :
		DiggerKilled();
		break;
	}
}

void EnemyManager::DiggerKilled()
{
	for (auto enemy : m_pEnemies)
	{
		enemy->DisableMovement();
		enemy->DisablePhysics();
	}
	m_StopSpawning = true;
}

void EnemyManager::NextLevel()
{
	for (auto enemy : m_pEnemies)
	{
		enemy->SetBackToSpawnPos();
		enemy->Disable();
	}
	m_EnemiesAlive = 0;
	if(m_CurrentMaxEnemies < m_MaxEnemies)
		m_CurrentMaxEnemies++;
	m_StopSpawning = false;
	m_CurrentEnemiesSpawned = 0;
}

void EnemyManager::EnemyDied()
{
	m_EnemiesAlive--;
	if (m_CurrentEnemiesSpawned == m_CurrentMaxEnemies && m_EnemiesAlive == 0)
	{
		m_GoToNextLevel = true;
	}
}

void EnemyManager::Reset()
{
	for (auto enemy : m_pEnemies)
	{
		enemy->SetBackToSpawnPos();
		enemy->Disable();
	}
	m_CurrentMaxEnemies = 5;
	m_StopSpawning = false;
	m_EnemiesAlive = 0;
	m_CurrentEnemiesSpawned = 0;
}

void EnemyManager::Update()
{
	if (m_GoToNextLevel)
	{
		m_pScene->NextLevel();
		m_GoToNextLevel = false;
	}
	if (m_StopSpawning)
	{
		m_CurrentTimeRestart += LittleEngine::Time::GetInstance().GetFixedTimeStep();
		if (m_CurrentTimeRestart >= m_TimeForRestart)
		{
			RestartAfterDeathDigger();
		}
		return;
	}

	if (m_CurrentEnemiesSpawned == m_CurrentMaxEnemies)
		return;

	m_CurrentTimeBetweenSpawns += LittleEngine::Time::GetInstance().GetFixedTimeStep();
	if (m_CurrentTimeBetweenSpawns >= m_TimeBetweenSpawns)
	{
		m_pEnemies[m_CurrentEnemiesSpawned]->Enable();
		m_CurrentEnemiesSpawned++;
		m_CurrentTimeBetweenSpawns = 0.f;
		m_EnemiesAlive++;
	}
}

void EnemyManager::RestartAfterDeathDigger()
{
	m_StopSpawning = false;
	for (auto enemy : m_pEnemies)
	{
		enemy->SetBackToSpawnPos();
		enemy->Disable();
	}
	m_CurrentEnemiesSpawned = 0;
	m_CurrentTimeBetweenSpawns = m_TimeBetweenSpawns - 0.5f;
	m_EnemiesAlive = 0;
	m_CurrentTimeRestart = 0.f;
}
