#include "MoneyBag.h"
#include "GameObject.h"
#include "AnimationComponent.h"
#include "Scene.h"
#include "SceneManager.h"
#include "PhysicsComponent.h"
#include "Enums.h"
#include "MoneyBag.h"
#include "UpdateComponent.h"
#include "Time.h"
#include "Gold.h"

MoneyBag::MoneyBag(float xPos, float yPos, int nodeWidth, int nodeHeight)
	: m_State{bagState::stuck}
	, m_BackGroundTouching{0}
	, m_IsFilterSet{false}
	, m_IsBeingPushed{false}
	, m_TimeToFallTwoStories{0.5f}
	, m_TimeFalling{0.f}
{
	using namespace LittleEngine;
	m_pBag = new GameObject();
	float scale{ 2.f };
	m_pAnim = new AnimationComponent("MoneyBagSheet.png",1,4,4,0.1f,scale,false,false);
	m_pBag->AddComponent(m_pAnim);
	m_pBag->SetTag("MoneyBag");
	PhysicsComponent::boxDefinition boxDef;
	boxDef.type = b2_dynamicBody;
	boxDef.density = 20.f;
	boxDef.offset = { 5.f,5.f };
	boxDef.position = { xPos,yPos };
	boxDef.height = nodeHeight / 2;
	boxDef.width = nodeWidth / 2 - 2;
	boxDef.userData = this;
	boxDef.gravityScale = 0.f;
	boxDef.friction = 0.2f;
	boxDef.linearDamping = 10.f;
	boxDef.fixedRotation = true;
	boxDef.categoryBits = uint16(objectCategories::MoneyBag);
	boxDef.maskBits = uint16(objectCategories::Digger) | uint16(objectCategories::Boundary) | uint16(objectCategories::Enemy);
	boxDef.drawDebug = true;
	m_pPhysics = new PhysicsComponent(boxDef);
	m_pBag->AddComponent(m_pPhysics);
	boxDef.type = b2_dynamicBody;
	boxDef.offset = { 5.f,30.f };
	boxDef.height = nodeHeight / 5;
	boxDef.width = nodeWidth / 2;
	boxDef.userData = this;
	boxDef.categoryBits = uint16(objectCategories::MoneyBag);
	boxDef.maskBits = uint16(objectCategories::Background) | uint16(objectCategories::Boundary) | uint16(objectCategories::Enemy);
	boxDef.isSensor = true;
	m_pPhysics->AddFixture(boxDef);
	auto update = [this]() {this->Update(); };
	UpdateComponent* pUpdate = new UpdateComponent{ update };
	m_pBag->AddComponent(pUpdate);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pBag,true);
	m_pBag->SetPosition(xPos, yPos);
	m_pGold = new Gold(xPos, yPos, nodeHeight / 2, nodeWidth / 2 -2);
}

MoneyBag::~MoneyBag()
{
	delete m_pGold;
}

void MoneyBag::AddBackgroundTouch()
{
	switch (m_State)
	{
	case MoneyBag::bagState::stuck:
		m_BackGroundTouching++;
		break;
	case MoneyBag::bagState::jiggling:
		break;
	case MoneyBag::bagState::gettingFree:
		m_BackGroundTouching++;
		m_TimeFalling = 0.f;
		m_State = bagState::stuck;
		m_pPhysics->SetGravityScale(0.f);
		break;
	case MoneyBag::bagState::falling:
		m_State = bagState::beingRemoved;
		break;
	default:
		break;
	}
}

void MoneyBag::SubstractBackgroundTouch()
{
	m_BackGroundTouching--;
	if (m_BackGroundTouching == 0)
	{
		m_State = bagState::jiggling;
		if(!m_IsBeingPushed)
			m_pAnim->Play(2);
		m_pPhysics->SetGravityScale(0.f);
	}
}

void MoneyBag::SetBeingPushed(bool beingPushed)
{
	m_IsBeingPushed = beingPushed;
}

bool MoneyBag::IsFalling() const
{
	return m_State == bagState::falling || m_State == bagState::gettingFree;
}

void MoneyBag::Disable()
{
	m_pBag->Disable();
	m_pGold->Disable();
}

void MoneyBag::RemoveFromScene()
{
	LittleEngine::SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(m_pBag);
	m_pGold->RemoveFromScene();
}


void MoneyBag::Update()
{
	switch (m_State)
	{
	case MoneyBag::bagState::stuck:
		break;
	case MoneyBag::bagState::jiggling:
		if (!m_pAnim->IsPlaying())
		{
			m_pPhysics->SetGravityScale(1.f);
			m_pPhysics->ApplyForce({ 0.f,10.f });
			m_State = bagState::gettingFree;
		}
		break;
	case MoneyBag::bagState::gettingFree:
		m_TimeFalling += LittleEngine::Time::GetInstance().GetFixedTimeStep();
		if (m_TimeFalling >= m_TimeToFallTwoStories)
		{
			m_State = bagState::falling;
		}
		break;
	case MoneyBag::bagState::beingRemoved:
	{
		LittleEngine::Float3 pos = m_pBag->GetTransform().GetPosition();
		m_pGold->SetPosition(pos.x, pos.y);
		m_pGold->Enable();
		LittleEngine::SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(m_pBag);
	}
		break;
	}
	
}



