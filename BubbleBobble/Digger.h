#pragma once
#include "ControlableObject.h"
#include "Structs.h"
#include <vector>

namespace LittleEngine
{
	class GameObject;
	class PhysicsComponent;
	class AnimationComponent;
	class RenderTextureComp;
}
class Digger : public ControlableObject
{
public:
	Digger();
	~Digger() = default;
	void SetVelocity(MoveState moveState) override;
	void HitByFallingBag();
	void SpawnDeathAnimation();
	void SpawnProjectile();
	void ProjectileCollision();
	void HitByEnemy();

	void Reset();
	void SetBackToSpawnPos();
	Digger(const Digger&) = delete;
	Digger(Digger&&) = delete;
	Digger& operator= (const Digger&) = delete;
	Digger& operator= (const Digger&&) = delete;

	LittleEngine::Float3 GetPosition() const;
private:
	LittleEngine::GameObject* m_pGameObject;
	LittleEngine::GameObject* m_pGraveObject;
	LittleEngine::GameObject* m_pProjectile;
	LittleEngine::PhysicsComponent* m_pPhysics;
	LittleEngine::PhysicsComponent* m_pPhysicsProjectile;
	LittleEngine::AnimationComponent* m_pAnimationAlive;
	LittleEngine::AnimationComponent* m_pAnimRip;
	LittleEngine::AnimationComponent* m_pAnimProj;
	LittleEngine::AnimationComponent* m_pAnimExplosion;
	std::vector<LittleEngine::RenderTextureComp*> m_pLives;
	LittleEngine::RenderTextureComp* m_pTextureDead;
	MoveState m_MoveState;
	float m_Velocity;
	float m_VelocityProjectile;
	float m_TimeDead;
	float m_TimeToRespawn;
	float m_TimeSinceProjectileFired;
	float m_ReloadTime;
	LittleEngine::Float2 m_StartPos;
	bool m_Died;
	bool m_IsProjectileFired;
	int m_Lives;

	void UpdateGrave();
	void UpdateProjectile();
	void Respawn();
};

