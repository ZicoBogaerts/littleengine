#include "Background.h"
#include "GameObject.h"
#include "RenderTexture.h"
#include "Scene.h"
#include "SceneManager.h"
#include "PhysicsComponent.h"
#include "Utils.h"
#include "Emerald.h"
#include "MoneyBag.h"
#include "Enums.h"
#include "Score.h"


Background::Background(const std::vector<std::string>& levelData)
	: m_LevelData{levelData}
	, m_TopGap{50}
	, m_Rows{10}
	, m_Cols{15}
	, m_NodeHeight{}
	, m_NodeWidth{}
{
}

Background::~Background()
{
	for (int i{ int(m_pMoneyBags.size()) - 1 }; i > -1; i--)
	{
		delete m_pMoneyBags[i];
	}
	m_pMoneyBags.clear();
}

void Background::AssembleLevel(int level, Score* pScore)
{
	using namespace LittleEngine;
	for (auto pBag : m_pMoneyBags)
	{
		pBag->RemoveFromScene();
	}
	for (auto pObject : m_pGameObjects)
	{
		pObject->Disable();
		SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(pObject);
	}
	m_pGameObjects.clear();

	m_NodeWidth = int(WINDOW_WIDTH / m_Cols);
	m_NodeWidth = int((WINDOW_WIDTH - (2 * m_NodeWidth)) / m_Cols - 1);
	m_NodeHeight = int((WINDOW_HEIGHT - m_TopGap) / m_Rows);
	m_NodeHeight = int(((WINDOW_HEIGHT - m_TopGap) - (2 * (m_NodeHeight / 5))) / m_Rows - 1);

	std::vector<std::pair<float, float>> bagPositions{};

	int amountOfEmeralds{};
	MakeEdgesBackground(level);
	for (int col{ 0 }; col < m_Cols; col++)
	{
		for (int row{ 0 }; row < m_Rows; row++)
		{
			int index = row * m_Cols + col;
			char c = m_LevelData[level].c_str()[index];
			switch (c)
			{
			case 'C':
			{
				float xPos = float(m_NodeWidth + col * m_NodeWidth);
				float yPos = float(m_NodeHeight / 5.f + m_TopGap + row * m_NodeHeight);
				FillBackgroundNode(xPos, yPos,level);
				GameObject* emerald = new GameObject();
				emerald->AddComponent(new RenderTextureComp("CEMERALD.png"));
				emerald->SetTag("Emerald");
				PhysicsComponent::boxDefinition boxDef;
				boxDef.type = b2_kinematicBody;
				boxDef.offset = { 6.f,0.f };
				boxDef.position = { xPos,yPos };
				boxDef.height = m_NodeHeight / 2;
				boxDef.width = m_NodeWidth / 2 + 4;
				boxDef.isSensor = true;
				PhysicsComponent* pPhysicsComp = new PhysicsComponent(boxDef);
				emerald->AddComponent(pPhysicsComp);
				SceneManager::GetInstance().GetActiveScene()->Add(emerald,true);
				m_pGameObjects.push_back(emerald);
				emerald->SetPosition(xPos, yPos);
				amountOfEmeralds++;

			}
			break;
			case 'B':
			{
				float xPos = float(m_NodeWidth + col * m_NodeWidth);
				float yPos = float(m_NodeHeight / 5.f + m_TopGap + row * m_NodeHeight);
				FillBackgroundNode(xPos, yPos,level);
				bagPositions.push_back({ xPos,yPos });
				
			}
			break;
			case ' ':
			{
				float xPos = float(m_NodeWidth + col * m_NodeWidth);
				float yPos = float(m_NodeHeight / 5.f + m_TopGap + row * m_NodeHeight);
				FillBackgroundNode(xPos,yPos,level);
			}
			break;
			}
		}
	}
	
	for (auto pos : bagPositions)
	{
		MoneyBag* moneyBag = new MoneyBag{ pos.first,pos.second,m_NodeWidth,m_NodeHeight };
		m_pMoneyBags.push_back(moneyBag);
	}
	pScore->SetAmountOfEmeralds(amountOfEmeralds);
}


void Background::FillBackgroundNode(float xPos, float yPos, int level, int lines, bool isHorizontalEdge, bool isVerticalEdge, bool isLeft)
{
	using namespace LittleEngine;
	std::string fileName{ "Background/Level" + std::to_string(level + 1) + "/"};
	int nrofRows{ 2 };
	int nrOfCols{ 4 };
	for (int i{ 0 }; i < lines; i++)
	{
		int textYpos = m_NodeHeight / 5 * i;
		for (int j{ 0 }; j < 8; j++)
		{
			GameObject* pObject = new GameObject();
			if (isHorizontalEdge)
				pObject->SetTag("Boundary");
			else
				pObject->SetTag("BackGround");
			switch (j)
			{
			case 0:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "TopLeft.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 1:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "TopMiddleLeft.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
		
			}
				break;
			case 2:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "TopMiddleRight.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (!isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 3:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "TopRight.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (!isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 4:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "BottomLeft.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 5:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "BottomMiddleLeft.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 6:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "BottomMiddleRight.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (!isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			case 7:
			{
				RenderTextureComp* pTexture = new RenderTextureComp(fileName + "BottomRight.png", 0, 0);
				pObject->AddComponent(pTexture);
				if (!isLeft && isVerticalEdge)
					pObject->SetTag("Boundary");
			}
				break;
			}
			int rowIdx = j / 4;
			int colIdx = j % 4;
			int localxPos = (m_NodeWidth / nrOfCols) * colIdx;
			int localyPos = ((m_NodeHeight / 5) / nrofRows) * rowIdx;
			PhysicsComponent::boxDefinition boxDef;
			boxDef.type = b2_staticBody;
			boxDef.density = 1.0f;
			boxDef.position = { xPos + localxPos,yPos + textYpos + localyPos };
			boxDef.friction = 3.0f;
			boxDef.height = m_NodeHeight / (5 * nrofRows);
			boxDef.width = m_NodeWidth / nrOfCols;
			boxDef.restitution = 0.5f;
			boxDef.gravityScale = 0.f;
			if(isHorizontalEdge || isVerticalEdge)
				boxDef.categoryBits = uint16(objectCategories::Boundary);
			else
				boxDef.categoryBits = uint16(objectCategories::Background);
			boxDef.maskBits = uint16(objectCategories::Digger) | uint16(objectCategories::Boundary) | uint16(objectCategories::MoneyBag) | uint16(objectCategories::Enemy);
			PhysicsComponent* pPhysicsComp = new PhysicsComponent(boxDef);
			pObject->AddComponent(pPhysicsComp);
			SceneManager::GetInstance().GetActiveScene()->Add(pObject,true);
			m_pGameObjects.push_back(pObject);
			pObject->SetPosition(xPos + localxPos, yPos + textYpos + localyPos);
		}
	}
}

void Background::MakeEdgesBackground(int level)
{
	using namespace LittleEngine;
	for (int col{ 0 }; col < 2; col++)
	{
		float xPos = float(m_NodeWidth + m_Cols * m_NodeWidth) * col;
		for (int row{ 0 }; row < m_Rows; row++)
		{
			float yPos = float(m_NodeHeight / 5.f + m_TopGap + row * m_NodeHeight);
			FillBackgroundNode(xPos, yPos, level, 5,false,true, col ? 0 : 1);
		}
	}
	for (int row{ 0 }; row < 2; row++)
	{
		float yPos = float(m_TopGap + (m_NodeHeight * m_Rows) * row);
		for (int col{ 0 }; col < m_Cols + 2; col++)
		{
			float xPos = float(col * m_NodeWidth);
			FillBackgroundNode(xPos, yPos, level, 2,true);
		}
	}
}
