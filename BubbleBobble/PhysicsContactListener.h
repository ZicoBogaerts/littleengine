#pragma once
#include <Box2D.h>

namespace LittleEngine
{
	class Subject;
}
class PhysicsContactListener : public b2ContactListener
{
public:
	PhysicsContactListener();
	~PhysicsContactListener();
	void BeginContact(b2Contact* contact) override;
	void EndContact(b2Contact* contact) override;
	LittleEngine::Subject* GetSubject() const { return m_pSubject; }
	PhysicsContactListener(const PhysicsContactListener&) = delete;
	PhysicsContactListener(PhysicsContactListener&&) = delete;
	PhysicsContactListener& operator= (const PhysicsContactListener&) = delete;
	PhysicsContactListener& operator= (const PhysicsContactListener&&) = delete;
private:
	LittleEngine::Subject* m_pSubject;
};

