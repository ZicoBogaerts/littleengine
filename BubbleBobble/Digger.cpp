#include "Digger.h"
#include "AnimationComponent.h"
#include "PhysicsComponent.h"
#include "UpdateComponent.h"
#include "GameObject.h"
#include "SceneManager.h"
#include "Scene.h"
#include "Utils.h"
#include "Enums.h"
#include "RenderTexture.h"
#include "Time.h"
#include "HighScores.h"


Digger::Digger()
	: m_MoveState{MoveState::Right}
	, m_Velocity{1.5f}
	, m_VelocityProjectile{3.f}
	, m_Died{false}
	, m_TimeDead{0.f}
	, m_TimeToRespawn{5.f}
	, m_TimeSinceProjectileFired{0.f}
	, m_ReloadTime{7.f}
	, m_IsProjectileFired{false}
	, m_Lives{3}
{
	using namespace LittleEngine;
	m_pGameObject = new GameObject();
	m_StartPos.x = 300.f;
	m_StartPos.y = 425.f;
	int nrRows{ 4 };
	int nrCols{ 6 };
	int nrFrames{ 24 };
	float secPerFrame = 0.1f;
	m_pAnimationAlive = new AnimationComponent("DiggingSheet.png", nrRows, nrCols, nrFrames, secPerFrame, 2.f, true);
	m_pGameObject->AddComponent(m_pAnimationAlive);
	m_pAnimationAlive->SetCurrentRow(2);
	m_pTextureDead = new RenderTextureComp("CBDIE.png", 0, 0, 2.f);
	m_pGameObject->AddComponent(m_pTextureDead);
	m_pTextureDead->Disable();
	PhysicsComponent::boxDefinition boxDef;
	boxDef.type = b2_dynamicBody;
	boxDef.density = 5.0f;
	boxDef.position = { m_StartPos.x ,m_StartPos.y };
	boxDef.drawOffset = { 5.f,8.f };
	boxDef.friction = 1.f;
	boxDef.height = 15;
	boxDef.width = 20;
	boxDef.restitution = 0.f;
	boxDef.gravityScale = 0.f;
	boxDef.linearDamping = 10.f;
	boxDef.fixedRotation = true;
	boxDef.categoryBits = uint16(objectCategories::Digger);
	boxDef.maskBits = uint16(objectCategories::MoneyBag) | uint16(objectCategories::Boundary) | uint16(objectCategories::Background) | uint16(objectCategories::Enemy);
	boxDef.drawDebug = true;
	boxDef.userData = this;
	m_pPhysics = new PhysicsComponent(boxDef);
	m_pGameObject->AddComponent(m_pPhysics);
	boxDef.type = b2_kinematicBody;
	boxDef.offset = { 15.f,0.f };
	boxDef.radius = 8;
	boxDef.shapeType = b2Shape::Type::e_circle;
	boxDef.isSensor = true;
	m_pPhysics->AddFixture(boxDef);
	m_pGameObject->SetTag("Digger");
	auto lambda = [this]() {this->UpdateProjectile(); };
	UpdateComponent* pUpdate = new UpdateComponent(lambda);
	m_pGameObject->AddComponent(pUpdate);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pGameObject);
	m_pGameObject->SetPosition(m_StartPos.x, m_StartPos.y);

	m_pGraveObject = new GameObject();
	m_pAnimRip = new AnimationComponent("GraveSheet.png", 1, 5, 5, 0.5f, 2.f, false,false);
	m_pGraveObject->AddComponent(m_pAnimRip);
	auto lambdaGrave = [this]() {this->UpdateGrave(); };
	pUpdate = new UpdateComponent(lambdaGrave);
	m_pGraveObject->AddComponent(pUpdate);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pGraveObject);
	m_pGraveObject->Disable();

	m_pProjectile = new GameObject();
	m_pProjectile->SetTag("FireBall");
	boxDef.type = b2_dynamicBody;
	boxDef.offset = { -2.f,-2.f };
	boxDef.drawOffset = { 0.f,0.f };
	boxDef.linearDamping = 0.f;
	boxDef.isSensor = true;
	boxDef.radius = 7;
	boxDef.categoryBits = uint16(objectCategories::Digger);
	boxDef.maskBits = uint16(objectCategories::Background) | uint16(objectCategories::Boundary) | uint16(objectCategories::Enemy);
	m_pPhysicsProjectile = new PhysicsComponent{ boxDef };
	m_pProjectile->AddComponent(m_pPhysicsProjectile);
	m_pAnimProj = new AnimationComponent("FireSheet.png", 1, 6, 6, 0.4f, 1.5f, false, false);
	m_pProjectile->AddComponent(m_pAnimProj);
	m_pAnimExplosion = new AnimationComponent("ExplosionSheet.png", 1, 6, 6, 0.1f, 1.5f, false, false);
	m_pProjectile->AddComponent(m_pAnimExplosion);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pProjectile);
	m_pProjectile->Disable();

	GameObject* pLives = new GameObject();
	float xPosition = 120.f;
	float yPosition = 15.f;
	float margin = 50.f;
	for (int i{ 0 }; i < m_Lives - 1; i++)
	{
		xPosition += margin * i;
		RenderTextureComp* pTex = new RenderTextureComp("CRLIFE.png", xPosition, yPosition,2.0f);
		m_pLives.push_back(pTex);
		pLives->AddComponent(pTex);
	}
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(pLives);
}

void Digger::SetVelocity(MoveState moveState)
{
	if (m_Died)
		return;
	m_MoveState = moveState;
	switch (moveState)
	{
	case ControlableObject::MoveState::Down:
		m_pPhysics->SetVelocity(b2Vec2{ 0,m_Velocity });
		m_pPhysics->SetRotation(LittleEngine::DegreesToRadians(90.f));
		m_pAnimationAlive->SetCurrentRow(int(MoveState::Down));
		break;
	case ControlableObject::MoveState::Left:
		m_pPhysics->SetVelocity(b2Vec2{ -m_Velocity,0 });
		m_pPhysics->SetRotation(LittleEngine::DegreesToRadians(180.f));
		m_pAnimationAlive->SetCurrentRow(int(MoveState::Left));
		break;
	case ControlableObject::MoveState::Right:
		m_pPhysics->SetVelocity(b2Vec2{ m_Velocity,0 });
		m_pPhysics->SetRotation(LittleEngine::DegreesToRadians(0.f));
		m_pAnimationAlive->SetCurrentRow(int(MoveState::Right));
		break;
	case ControlableObject::MoveState::Up:
		m_pPhysics->SetVelocity(b2Vec2{ 0,-m_Velocity });
		m_pPhysics->SetRotation(LittleEngine::DegreesToRadians(-90.f));
		m_pAnimationAlive->SetCurrentRow(int(MoveState::Up));
		break;
	default:
		break;
	}
}

void Digger::HitByFallingBag()
{
	m_pAnimationAlive->Disable();
	m_Died = true;
	m_pTextureDead->Enable();
	b2Filter filter{};
	filter.categoryBits = uint16(objectCategories::Digger);
	filter.maskBits = 0;
	m_pPhysics->SetFilterData(filter, 1);
	m_pPhysics->SetGravityScale(1.f);
}

void Digger::SpawnDeathAnimation()
{
	if (m_Died)
	{
		m_pGameObject->Disable();
		m_pGraveObject->SetPosition(m_pGameObject->GetTransform().GetPosition().x, m_pGameObject->GetTransform().GetPosition().y);
		m_pGraveObject->Enable();
		m_pAnimRip->Play(1);
	}
}

void Digger::SpawnProjectile()
{
	if (m_IsProjectileFired)
		return;
	m_pProjectile->Enable();
	m_pAnimExplosion->Disable();
	LittleEngine::Float3 pos = m_pGameObject->GetTransform().GetPosition();
	m_pAnimProj->Play(0);
	m_IsProjectileFired = true;
	m_TimeSinceProjectileFired = 0.f;

	switch (m_MoveState)
	{
	case ControlableObject::MoveState::Down:
		m_pPhysicsProjectile->SetBodyPosition(pos.x + 10.f, pos.y + 25.f);
		m_pPhysicsProjectile->SetVelocity({ 0.f,m_VelocityProjectile });
		break;
	case ControlableObject::MoveState::Left:
		m_pPhysicsProjectile->SetBodyPosition(pos.x - 10.f, pos.y + 10.f);
		m_pPhysicsProjectile->SetVelocity({ -m_VelocityProjectile ,0.f });
		break;
	case ControlableObject::MoveState::Right:
		m_pPhysicsProjectile->SetBodyPosition(pos.x + 25.f, pos.y + 10.f);
		m_pPhysicsProjectile->SetVelocity({ m_VelocityProjectile ,0.f });
		break;
	case ControlableObject::MoveState::Up:
		m_pPhysicsProjectile->SetBodyPosition(pos.x + 5.f, pos.y - 5.f);
		m_pPhysicsProjectile->SetVelocity({ 0.f,-m_VelocityProjectile });
		break;
	default:
		break;
	}
}

void Digger::ProjectileCollision()
{
	m_pAnimProj->Disable();
	m_pPhysicsProjectile->Disable();
	m_pAnimExplosion->Enable();
	m_pAnimExplosion->Play(1);
}

void Digger::HitByEnemy()
{
	m_pAnimationAlive->Disable();
	m_Died = true;
	m_pTextureDead->Enable();
	m_pGraveObject->SetPosition(m_pGameObject->GetTransform().GetPosition().x, m_pGameObject->GetTransform().GetPosition().y);
	m_pGraveObject->Enable();
	m_pAnimRip->Play(1);
	b2Filter filter{};
	filter.categoryBits = uint16(objectCategories::Digger);
	filter.maskBits = 0;
	m_pPhysics->SetFilterData(filter, 0);
	m_pPhysics->SetFilterData(filter, 1);
}

void Digger::Reset()
{
	Respawn();
	m_Lives = 3;
	for (int i{ 0 }; i < 2; i++)
	{
		m_pLives[i]->Enable();
	}
}

void Digger::SetBackToSpawnPos()
{
	m_pPhysics->SetBodyPosition(m_StartPos.x, m_StartPos.y);
	m_pGameObject->SetPosition(m_StartPos.x, m_StartPos.y);
	m_MoveState = MoveState::Right;
	m_pAnimationAlive->SetCurrentRow(int(MoveState::Right));
}

LittleEngine::Float3 Digger::GetPosition() const
{
	return LittleEngine::Float3{m_pGameObject->GetTransform().GetPosition()};
}

void Digger::UpdateGrave()
{
	if (!m_pAnimRip->IsPlaying())
	{
		m_pAnimRip->SetCurrentFrame(4);
		m_pGameObject->Disable();
	}
	m_TimeDead += LittleEngine::Time::GetInstance().GetFixedTimeStep();
	if (m_TimeDead >= m_TimeToRespawn)
	{
		m_Lives--;
		if (m_Lives <= 0)
		{
			LittleEngine::SceneManager::GetInstance().SetActiveScene(int(Scenes::StartScene));
			HighScores::GetInstance().AddHighScore();
			return;
		}
		Respawn();
		m_pLives[m_Lives -1]->Disable();

	}
}

void Digger::UpdateProjectile()
{
	if (m_IsProjectileFired)
	{
		m_TimeSinceProjectileFired += LittleEngine::Time::GetInstance().GetFixedTimeStep();
		if (m_TimeSinceProjectileFired >= m_ReloadTime)
		{
			m_IsProjectileFired = false;
		}
		if (!m_pAnimExplosion->IsPlaying())
		{
			m_pAnimExplosion->Disable();
		}
	}
}

void Digger::Respawn()
{
	m_pAnimationAlive->Enable();
	m_MoveState = MoveState::Right;
	m_pAnimationAlive->SetCurrentRow(int(MoveState::Right));
	m_pTextureDead->Disable();
	m_pGraveObject->Disable();
	m_pPhysics->Enable();
	b2Filter filter{};
	filter.categoryBits = uint16(objectCategories::Digger);
	filter.maskBits = uint16(objectCategories::MoneyBag) | uint16(objectCategories::Boundary) | uint16(objectCategories::Background) | uint16(objectCategories::Enemy);
	m_pPhysics->SetFilterData(filter, 0);
	m_pPhysics->SetFilterData(filter, 1);
	m_pPhysics->SetGravityScale(0.f);
	m_pPhysics->SetBodyPosition(m_StartPos.x, m_StartPos.y);
	m_pPhysics->SetRotation(LittleEngine::DegreesToRadians(0.f));
	m_pGameObject->SetPosition(m_StartPos.x, m_StartPos.y);
	m_pGameObject->Enable();
	m_pTextureDead->Disable();
	m_pAnimRip->SetCurrentFrame(0);
	m_Died = false;
	m_IsProjectileFired = false;
	m_TimeSinceProjectileFired = 0.f;
	m_pProjectile->Disable();
	m_pAnimExplosion->Disable();
	m_TimeDead = 0.f;
}
