#include "Score.h"
#include "GameObject.h"
#include "Text.h"
#include "Scene.h"
#include "SceneManager.h"
#include "ResourceManager.h"
#include "Enums.h"
#include "MainScene.h"
#include "UpdateComponent.h"

Score::Score(MainScene* pScene)
	: m_Score{0}
	, m_EmeraldsInRow{0}
	, m_EmeraldScore{25}
	, m_EmeraldBonus{250}
	, m_GoldScore{250}
	, m_EnemyScore{250}
	, m_AmountOfEmeralds{0}
	, m_pScene{pScene}
	, m_GoToNextLevel{false}
{
	using namespace LittleEngine;
	GameObject* pObject = new GameObject();
	float xPos{ 20.f };
	float yPos{ 20.f };
	m_pText = new Text("0000", ResourceManager::GetInstance().GetFont("Lingua.otf"), xPos, yPos);
	m_pText->SetColor(SDL_Color{ 0,255,0,255 });
	pObject->AddComponent(m_pText);
	auto update = [this]() {this->Update(); };
	UpdateComponent* pUpdate = new UpdateComponent{ update };
	pObject->AddComponent(pUpdate);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(pObject);
}

void Score::EmeraldPickedUp()
{
	m_EmeraldsInRow++;
	if (m_EmeraldsInRow < 8)
	{
		m_Score += m_EmeraldScore;
	}
	else
	{
		m_Score += m_EmeraldBonus + m_EmeraldScore;
		m_EmeraldsInRow = 0;
	}
	m_AmountOfEmeralds--;
	if (m_AmountOfEmeralds == 0)
	{
		m_GoToNextLevel = true;
	}
	m_pText->SetText(std::to_string(m_Score));
}

void Score::GoldPickedUp()
{
	m_EmeraldsInRow = 0;
	m_Score += m_GoldScore;
	m_pText->SetText(std::to_string(m_Score));
}

void Score::EnemyKilled()
{
	m_EmeraldsInRow = 0;
	m_Score += m_EnemyScore;
	m_pText->SetText(std::to_string(m_Score));
}

void Score::Reset()
{
	m_EmeraldsInRow = 0;
	m_Score = 0;
	m_pText->SetText(std::to_string(m_Score));
}

void Score::SetAmountOfEmeralds(int amountOfEmeralds)
{
	m_AmountOfEmeralds = amountOfEmeralds;
}

void Score::OnNotify(int event)
{
	switch (event)
	{
	case int(Events::emeraldPickedUp):
		EmeraldPickedUp();
		break;
	case int(Events::goldPickedUp):
		GoldPickedUp();
		break;
	case int(Events::enemyKilled):
		EnemyKilled();
		break;
	case int(Events::emeraldPickedUpHobbin):
		m_AmountOfEmeralds--;
		if (m_AmountOfEmeralds == 0)
		{
			m_GoToNextLevel = true;
		}
		break;
	}
}

void Score::Update()
{
	if (m_GoToNextLevel)
	{
		m_pScene->NextLevel();
		m_GoToNextLevel = false;
		m_EmeraldsInRow = 0;
	}
}

