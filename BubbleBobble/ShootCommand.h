#pragma once
#include "Command.h"
#include "Digger.h"
class ShootCommand : public LittleEngine::Command
{
public:
	ShootCommand(Digger* pDigger);
	void Execute() override;
private:
	Digger* m_pObject;
};

