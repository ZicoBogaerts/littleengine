#include "Gold.h"
#include "GameObject.h"
#include "UpdateComponent.h"
#include "Time.h"
#include "AnimationComponent.h"
#include "PhysicsComponent.h"
#include "Enums.h"
#include "Scene.h"
#include "SceneManager.h"

Gold::Gold(float xPos, float yPos, int width, int height)
	: m_LifeTime{10.f}
	, m_TimeAlive{0.f}
	, m_PickedUp{false}
{
	using namespace LittleEngine;
	m_pObject = new GameObject();
	m_pObject->SetTag("Gold");
	PhysicsComponent::boxDefinition boxDef;
	boxDef.type = b2_staticBody;
	boxDef.density = 20.f;
	boxDef.offset = { 5.f,5.f };
	boxDef.position = { xPos,yPos + 15.f };
	boxDef.height = height / 2;
	boxDef.width = width;
	boxDef.userData = m_pObject;
	boxDef.gravityScale = 0.f;
	boxDef.linearDamping = 10.f;
	boxDef.fixedRotation = true;
	boxDef.categoryBits = uint16(objectCategories::MoneyBag);
	boxDef.maskBits = uint16(objectCategories::Digger) | uint16(objectCategories::Boundary);
	boxDef.isSensor = true;
	m_pPhysics = new PhysicsComponent{ boxDef,true };
	m_pObject->AddComponent(m_pPhysics);
	m_pAnim = new AnimationComponent("GoldSheet.png", 1, 3, 3, 0.1f, 1.5f, false, false);
	m_pObject->AddComponent(m_pAnim);
	m_pAnim->Play(1);

	auto lambda = [this]() {this->Update(); };
	UpdateComponent* pUpdate = new UpdateComponent(lambda);
	m_pObject->AddComponent(pUpdate);
	SceneManager::GetInstance().GetActiveScene()->Add(m_pObject,true);
	m_pObject->Disable();
}

Gold::~Gold()
{
}

void Gold::Enable()
{
	if (m_pObject != nullptr)
		m_pObject->Enable();
}

void Gold::Disable()
{
	if(m_pObject != nullptr)
		m_pObject->Disable();
}

void Gold::SetPosition(float xPos, float yPos)
{
	float y = yPos + 15.f;
	m_pObject->SetPosition(xPos, y);
	m_pPhysics->SetBodyPosition(xPos, y);
}

void Gold::RemoveFromScene()
{
	if(m_pObject != nullptr)
		LittleEngine::SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(m_pObject);
}

void Gold::Update()
{
	if (!m_pAnim->IsPlaying())
	{
		m_pAnim->SetCurrentFrame(2);
	}
	m_TimeAlive += LittleEngine::Time::GetInstance().GetFixedTimeStep();
	if (m_TimeAlive >= m_LifeTime || m_PickedUp)
	{
		LittleEngine::SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(m_pObject);
		m_pObject = nullptr;
	}
}
