#pragma once
#include "Command.h"
#include "ControlableObject.h"

class MoveCommand : public LittleEngine::Command
{
public:
	MoveCommand(ControlableObject* pObject, ControlableObject::MoveState state);
	void Execute() override;
private:
	ControlableObject* m_pObject;
	ControlableObject::MoveState m_State;
};

