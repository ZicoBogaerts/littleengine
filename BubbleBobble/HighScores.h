#pragma once
#include "Singleton.h"
#include <vector>


class Score;
namespace LittleEngine
{
	class Text;
	class GameObject;
};
class HighScores : public LittleEngine::Singleton<HighScores>
{
public:
	HighScores();
	~HighScores();
	void AddHighScore();
	void Enable();
	void Disable();
	void SetScoreReference(Score* pScore);
	void ReadHighScores();
	void AddInitials();
private:
	std::vector<LittleEngine::Text*> m_pHighScoreTexts;
	std::vector<int> m_HighScores;
	std::vector<LittleEngine::Text*> m_pInitials;
	LittleEngine::GameObject* m_pGameObject;
	int m_NrOfHighScores;
	Score* m_pScore;
};

