#pragma once
#include "Scene.h"

class Digger;
class EnemyManager;
class PhysicsContactListener;
class Score;
class Background;

class MainScene final
{
public:
	~MainScene();
	MainScene(const std::string& name);
	void NextLevel();

	MainScene(const MainScene&) = delete;
	MainScene(MainScene&&) = delete;
	MainScene& operator= (const MainScene&) = delete;
	MainScene& operator= (const MainScene&&) = delete;

private:
	PhysicsContactListener* m_pContactListener;
	Digger* m_pDigger;
	EnemyManager* m_pEnemyManager;
	Score* m_pScore;
	Background* m_pBackground;
	int m_CurrentLevel;

	void LoadLevels();
	void Initialize();
	void Update();
	void Render() const;
	void SceneActivated();
};

