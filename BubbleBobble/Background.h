#pragma once
#include <string>
#include <vector>

class MoneyBag;
class Emerald;
class Score;

namespace LittleEngine
{
	class GameObject;
}
class Background
{
public:
	Background(const std::vector<std::string>& levelData);
	~Background();
	void AssembleLevel(int level, Score* pScore);
	Background(const Background&) = delete;
	Background(Background&&) = delete;
	Background& operator= (const Background&) = delete;
	Background& operator= (const Background&&) = delete;
private:
	int m_TopGap;
	int m_Rows;
	int m_Cols;
	int m_NodeWidth;
	int m_NodeHeight;
	std::vector<std::string> m_LevelData;
	std::vector<MoneyBag*> m_pMoneyBags;
	std::vector<LittleEngine::GameObject*> m_pGameObjects;

	void FillBackgroundNode(float xPos, float yPos, int level, int lines = 5, bool isEdge = false,bool isVerticalEdge = false ,bool isLeft = false);
	void MakeEdgesBackground(int level);
};

