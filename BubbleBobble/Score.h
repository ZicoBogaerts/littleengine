#pragma once
#include "Singleton.h"
#include "Observer.h"

namespace LittleEngine
{
	class Text;
}
class MainScene;

class Score : public LittleEngine::Observer
{
public:
	Score(MainScene* pScene);
	void EmeraldPickedUp();
	void GoldPickedUp();
	void EnemyKilled();
	void Reset();
	void SetAmountOfEmeralds(int amountOfEmeralds);
	void OnNotify(int event) override;
	int GetScore() const { return m_Score; }

	Score(const Score&) = delete;
	Score(Score&&) = delete;
	Score& operator= (const Score&) = delete;
	Score& operator= (const Score&&) = delete;
private:
	int m_Score;
	int m_EmeraldsInRow;
	int m_EmeraldScore;
	int m_EmeraldBonus;
	int m_GoldScore;
	int m_EnemyScore;
	int m_AmountOfEmeralds;
	LittleEngine::Text* m_pText;
	MainScene* m_pScene;
	bool m_GoToNextLevel;

	void Update();
};

