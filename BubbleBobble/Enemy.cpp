#include "Enemy.h"
#include "GameObject.h"
#include "AnimationComponent.h"
#include "PhysicsComponent.h"
#include "UpdateComponent.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Enums.h"
#include "Digger.h"
#include "Utils.h"
#include "Time.h"

Enemy::Enemy(float xPos, float yPos, Digger* pDigger)
	: m_State{enemyState::Nobbin}
	, m_IsPlayerControlled{false}
	, m_StopMovement{false}
	, m_IsHitByBag{false}
	, m_IsDisabled{false}
	, m_pDigger{pDigger}
	, m_BackgroundTouches{}
	, m_CurrentMoveState{MoveState::Left}
	, m_Velocity{1.2f}
	, m_TimeBetweenSwitch{5.f}
	, m_CurrentSwitchTime{0.f}
	, m_SpawnXPos{xPos}
	, m_SpawnYPos{yPos}
	, m_ChanceForSwitch{10}
{
	using namespace LittleEngine;
	m_pNobbin = new GameObject();
	m_pNobbin->SetTag("Nobbin");
	AnimationComponent* pAnimNobbin = new AnimationComponent("AlienWalkSheet.png", 1, 4, 4, 0.15f,1.5f);
	m_pNobbin->AddComponent(pAnimNobbin);
	auto updateNobbin = [this]() {this->UpdateNobbin(); };
	UpdateComponent* pUpdateNobbin = new UpdateComponent{ updateNobbin };
	m_pNobbin->AddComponent(pUpdateNobbin);
	PhysicsComponent::boxDefinition boxDefin;
	boxDefin.type = b2_dynamicBody;
	boxDefin.density = 5.0f;
	boxDefin.position = { xPos,yPos };
	boxDefin.drawOffset = { 5.f,5.f };
	boxDefin.friction = 0.2f;
	boxDefin.height = 13;
	boxDefin.width = 13;
	boxDefin.restitution = 0.f;
	boxDefin.gravityScale = 0.f;
	boxDefin.linearDamping = 10.f;
	boxDefin.fixedRotation = true;
	boxDefin.categoryBits = uint16(objectCategories::Enemy);
	boxDefin.maskBits = uint16(objectCategories::MoneyBag) | uint16(objectCategories::Boundary) | uint16(objectCategories::Background) | uint16(objectCategories::Digger);
	boxDefin.drawDebug = true;
	boxDefin.userData = this;
	m_pPhysicsNobbin = new PhysicsComponent{ boxDefin };
	m_pNobbin->AddComponent(m_pPhysicsNobbin);
	float offset{ 16.f };
	//Left box sensor
	boxDefin.density = int(MoveState::Left);
	boxDefin.type = b2_kinematicBody;
	boxDefin.offset = { -offset,0.f };
	boxDefin.width = 5;
	boxDefin.height = 13;
	boxDefin.isSensor = true;
	m_pPhysicsNobbin->AddFixture(boxDefin);
	//Right box sensor
	boxDefin.density = int(MoveState::Right);
	boxDefin.offset = { offset,0.f };
	boxDefin.width = 5;
	boxDefin.height = 13;
	boxDefin.isSensor = true;
	m_pPhysicsNobbin->AddFixture(boxDefin);
	//Upper box sensor
	boxDefin.density = int(MoveState::Up);
	boxDefin.offset = { 0.f,-offset };
	boxDefin.width = 15;
	boxDefin.height = 5;
	boxDefin.isSensor = true;
	m_pPhysicsNobbin->AddFixture(boxDefin);
	//Lower box sensor
	boxDefin.density = int(MoveState::Down);
	boxDefin.offset = { 0.f,offset };
	boxDefin.width = 15;
	boxDefin.height = 5;
	boxDefin.isSensor = true;
	m_pPhysicsNobbin->AddFixture(boxDefin);

	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pNobbin);
	m_pNobbin->SetPosition(xPos, yPos);
	//m_pNobbin->Disable();
	m_MoveDirections[int(MoveState::Left)] = true;
	m_MoveDirections[int(MoveState::Right)] = true;
	m_MoveDirections[int(MoveState::Down)] = true;
	m_MoveDirections[int(MoveState::Up)] = true;

	m_pHobbin = new GameObject();
	m_pHobbin->SetTag("Hobbin");
	AnimationComponent* pAnimHobbin = new AnimationComponent("HobbinSheet.png", 1, 4, 4, 0.5f,1.5f);
	m_pHobbin->AddComponent(pAnimHobbin);
	PhysicsComponent::boxDefinition boxDef;
	boxDef.type = b2_dynamicBody;
	boxDef.density = 20.0f;
	boxDef.position = { xPos ,yPos };
	boxDef.drawOffset = { 5.f,0.f };
	boxDef.friction = 0.2f;
	boxDef.height = 13;
	boxDef.width = 13;
	boxDef.restitution = 0.f;
	boxDef.gravityScale = 0.f;
	boxDef.linearDamping = 10.f;
	boxDef.fixedRotation = true;
	boxDef.categoryBits = uint16(objectCategories::Enemy);
	boxDef.maskBits = uint16(objectCategories::MoneyBag) | uint16(objectCategories::Boundary) | uint16(objectCategories::Background) | uint16(objectCategories::Digger);
	boxDef.drawDebug = true;
	boxDef.userData = this;
	m_pPhysicsHobbin = new PhysicsComponent{ boxDef };
	m_pHobbin->AddComponent(m_pPhysicsHobbin);
	boxDef.type = b2_kinematicBody;
	boxDef.offset = { -10.f,0.f };
	boxDef.radius = 8;
	boxDef.shapeType = b2Shape::Type::e_circle;
	boxDef.isSensor = true;
	m_pPhysicsHobbin->AddFixture(boxDef);
	auto updateHobbin = [this]() {this->UpdateHobbin(); };
	UpdateComponent* pUpdateHobbin = new UpdateComponent{ updateHobbin };
	m_pHobbin->AddComponent(pUpdateHobbin);
	SceneManager::GetInstance().GetScene(int(Scenes::MainScene))->Add(m_pHobbin);
	m_pHobbin->SetPosition(xPos, yPos);
	m_pHobbin->Disable();

}

void Enemy::SetVelocity(MoveState moveState)
{
	switch (moveState)
	{
	case ControlableObject::MoveState::Down:
		if (m_State == enemyState::Nobbin)
			m_pPhysicsNobbin->SetVelocity(b2Vec2{ 0,m_Velocity });
		else
		{
			m_pPhysicsHobbin->SetVelocity(b2Vec2{ 0,m_Velocity });
			m_pPhysicsHobbin->SetRotation(LittleEngine::DegreesToRadians(-90.f));
		}
		break;
	case ControlableObject::MoveState::Left:
		if (m_State == enemyState::Nobbin)
			m_pPhysicsNobbin->SetVelocity(b2Vec2{ -m_Velocity,0 });
		else
		{
			m_pPhysicsHobbin->SetVelocity(b2Vec2{ -m_Velocity,0 });
			m_pPhysicsHobbin->SetRotation(LittleEngine::DegreesToRadians(0.f));
		}
		break;
	case ControlableObject::MoveState::Right:
		if (m_State == enemyState::Nobbin)
			m_pPhysicsNobbin->SetVelocity(b2Vec2{ m_Velocity,0 });
		else
		{
			m_pPhysicsHobbin->SetVelocity(b2Vec2{ m_Velocity,0 });
			m_pPhysicsHobbin->SetRotation(LittleEngine::DegreesToRadians(180.f));
		}
		break;
	case ControlableObject::MoveState::Up:
		if (m_State == enemyState::Nobbin)
			m_pPhysicsNobbin->SetVelocity(b2Vec2{ 0,-m_Velocity });
		else
		{
			m_pPhysicsHobbin->SetVelocity(b2Vec2{ 0,-m_Velocity });
			m_pPhysicsHobbin->SetRotation(LittleEngine::DegreesToRadians(90.f));
		}
		break;
	default:
		break;
	}
}

void Enemy::AddBackgroundTouch(int direction)
{
	m_BackgroundTouches[direction]++;
	m_MoveDirections[direction] = false;
}

void Enemy::SubstractBackgroundTouch(int direction)
{
	m_BackgroundTouches[direction]--;
	if (m_BackgroundTouches[direction] == 0)
	{
		m_MoveDirections[direction] = true;
		CheckForChangeMoveState(direction);
	}
}

void Enemy::DisableMovement()
{
	m_StopMovement = true;
}

void Enemy::DisablePhysics()
{
	if (m_State == enemyState::Nobbin)
		m_pPhysicsNobbin->Disable();
	else
		m_pPhysicsHobbin->Disable();
}

void Enemy::Disable()
{
	m_pHobbin->Disable();
	m_pNobbin->Disable();
	m_IsDisabled = true;
}

void Enemy::Enable()
{
	m_pNobbin->Enable();
	m_IsDisabled = false;
}

void Enemy::SetBackToSpawnPos()
{
	m_pHobbin->SetPosition(m_SpawnXPos, m_SpawnYPos);
	m_pNobbin->SetPosition(m_SpawnXPos, m_SpawnYPos);
	m_pPhysicsNobbin->SetBodyPosition(m_SpawnXPos, m_SpawnYPos);
	m_pPhysicsHobbin->SetBodyPosition(m_SpawnXPos, m_SpawnYPos);
	m_StopMovement = false;
	m_IsHitByBag = false;
	m_State = enemyState::Nobbin;
}

void Enemy::HitByBag()
{
	m_StopMovement = true;
	if (m_State == enemyState::Nobbin)
		m_pPhysicsNobbin->SetGravityScale(1.f);
	else
		m_pPhysicsHobbin->SetGravityScale(1.f);
	m_IsHitByBag = true;
}

void Enemy::UpdateHobbin()
{
	if (m_IsPlayerControlled || m_StopMovement)
		return;

	m_CurrentSwitchTime += LittleEngine::Time::GetInstance().GetFixedTimeStep();
	if (m_CurrentSwitchTime >= m_TimeBetweenSwitch)
	{
		int random = rand() % 100;
		m_CurrentSwitchTime = 0.f;
		if (random < m_ChanceForSwitch)
		{
			m_pHobbin->Disable();
			m_pNobbin->Enable();
			LittleEngine::Float3 pos = m_pHobbin->GetTransform().GetPosition();
			m_pPhysicsNobbin->SetBodyPosition(pos.x, pos.y);
			m_State = enemyState::Nobbin;
			m_ChanceForSwitch = 10;
			return;
		}
		m_ChanceForSwitch++;
	}

	LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
	LittleEngine::Float3 pos = m_pHobbin->GetTransform().GetPosition();

	float xDiff = diggerPos.x - pos.x;
	float yDiff = diggerPos.y - pos.y;
	float threshhold{ 20.f };
	if (abs(xDiff) > threshhold || abs(yDiff) < threshhold)
	{
		if (xDiff > 0)
			m_CurrentMoveState = MoveState::Right;
		else
			m_CurrentMoveState = MoveState::Left;
	}
	else 
	{
		if (yDiff > 0)
			m_CurrentMoveState = MoveState::Down;
		else
			m_CurrentMoveState = MoveState::Up;
	}
	SetVelocity(m_CurrentMoveState);
}

void Enemy::UpdateNobbin()
{
	if (m_IsPlayerControlled || m_StopMovement)
		return;

	m_CurrentSwitchTime += LittleEngine::Time::GetInstance().GetFixedTimeStep();
	if (m_CurrentSwitchTime >= m_TimeBetweenSwitch)
	{
		int random = rand() % 100;
		m_CurrentSwitchTime = 0.f;
		if (random < m_ChanceForSwitch)
		{
			m_pNobbin->Disable();
			m_pHobbin->Enable();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			m_pPhysicsHobbin->SetBodyPosition(pos.x, pos.y);
			m_State = enemyState::Hobbin;
			m_ChanceForSwitch = 10;
			return;
		}
		m_ChanceForSwitch++;
	}
	if (!m_MoveDirections[int(m_CurrentMoveState)])
	{
		SetMoveState();
	}
	SetVelocity(m_CurrentMoveState);
	
}

void Enemy::SetMoveState()
{
	std::vector<int> possibleDirections{ 0,1,2,3 };
	for (int i{ 0 }; i < 4; i++)
	{
		if (!m_MoveDirections[i])
		{
			possibleDirections.erase(std::find(possibleDirections.begin(), possibleDirections.end(), i));
		}
	}
	if (possibleDirections.size() > 1)
	{
		int oppositeOfCurrentDirection{ 0 };
		switch (m_CurrentMoveState)
		{
		case ControlableObject::MoveState::Down:
			oppositeOfCurrentDirection = 3;
			break;
		case ControlableObject::MoveState::Left:
			oppositeOfCurrentDirection = 2;
			break;
		case ControlableObject::MoveState::Right:
			oppositeOfCurrentDirection = 1;
			break;
		case ControlableObject::MoveState::Up:
			oppositeOfCurrentDirection = 0;
			break;
		default:
			break;
		}
		auto it = std::find(possibleDirections.begin(), possibleDirections.end(), oppositeOfCurrentDirection);
		if(it != possibleDirections.end())
			possibleDirections.erase(it);
		if (possibleDirections.size() > 1)
		{
			LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			if (possibleDirections[0] == 0 || possibleDirections[0] == 3)
			{
				float yDiff = diggerPos.y - pos.y;
				if (yDiff > 0)
					m_CurrentMoveState = MoveState::Down;
				else
					m_CurrentMoveState = MoveState::Up;
			}
			else
			{
				float xDiff = diggerPos.x - pos.x;
				if (xDiff > 0)
					m_CurrentMoveState = MoveState::Right;
				else
					m_CurrentMoveState = MoveState::Left;
			}
			return;
		}
	}
	if(!possibleDirections.empty())
		m_CurrentMoveState = static_cast<MoveState>(possibleDirections[0]);
}

void Enemy::CheckForChangeMoveState(int direction)
{
	if (direction == int(m_CurrentMoveState))
		return;
	switch (m_CurrentMoveState)
	{
	case ControlableObject::MoveState::Down:
		if (direction != int(MoveState::Up))
		{
			LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			float xDiff = diggerPos.x - pos.x;
			float yDiff = diggerPos.y - pos.y;
			if (abs(yDiff) > abs(xDiff))
				return;
			if (direction == int(MoveState::Left) && xDiff < 0)
				m_CurrentMoveState = MoveState::Left;
			else if (direction == int(MoveState::Right) && xDiff > 0)
				m_CurrentMoveState = MoveState::Right;
		}
		break;
	case ControlableObject::MoveState::Left:
		if (direction != int(MoveState::Right))
		{
			LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			float xDiff = diggerPos.x - pos.x;
			float yDiff = diggerPos.y - pos.y;
			if (abs(xDiff) > abs(yDiff))
				return;
			if (direction == int(MoveState::Up) && yDiff < 0)
				m_CurrentMoveState = MoveState::Up;
			else if (direction == int(MoveState::Down) && yDiff > 0)
				m_CurrentMoveState = MoveState::Down;

		}
		break;
	case ControlableObject::MoveState::Right:
		if (direction != int(MoveState::Left))
		{
			LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			float xDiff = diggerPos.x - pos.x;
			float yDiff = diggerPos.y - pos.y;
			if (abs(xDiff) > abs(yDiff))
				return;
			if (direction == int(MoveState::Up) && yDiff < 0)
				m_CurrentMoveState = MoveState::Up;
			else if (direction == int(MoveState::Down) && yDiff > 0)
				m_CurrentMoveState = MoveState::Down;

		}
		break;
	case ControlableObject::MoveState::Up:
		if (direction != int(MoveState::Down))
		{
			LittleEngine::Float3 diggerPos = m_pDigger->GetPosition();
			LittleEngine::Float3 pos = m_pNobbin->GetTransform().GetPosition();
			float xDiff = diggerPos.x - pos.x;
			float yDiff = diggerPos.y - pos.y;
			if (abs(xDiff) > abs(yDiff))
				return;
			if (direction == int(MoveState::Left) && xDiff < 0)
				m_CurrentMoveState = MoveState::Left;
			else if (direction == int(MoveState::Right) && xDiff > 0)
				m_CurrentMoveState = MoveState::Right;
		}
		break;
	default:
		break;
	}
}
