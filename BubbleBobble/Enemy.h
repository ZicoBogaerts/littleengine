#pragma once
#include "ControlableObject.h"
#include <map>

namespace LittleEngine
{
	class GameObject;
	class PhysicsComponent;
}
class Digger;

class Enemy : public ControlableObject
{
public:
	Enemy(float xPos, float yPos, Digger* pDigger);
	void SetVelocity(MoveState moveState) override;

	void AddBackgroundTouch(int direction);
	void SubstractBackgroundTouch(int direction);
	void DisableMovement();
	void DisablePhysics();
	void Disable();
	void Enable();
	void SetBackToSpawnPos();

	void HitByBag();
	bool GetHitByBag() const { return m_IsHitByBag; }
	bool IsDisabled() const { return m_IsDisabled; }

	Enemy(const Enemy&) = delete;
	Enemy(Enemy&&) = delete;
	Enemy& operator= (const Enemy&) = delete;
	Enemy& operator= (const Enemy&&) = delete;
private:
	enum class enemyState
	{
		Nobbin = 0,
		Hobbin = 1,
	};
	enemyState m_State;
	//Nobbin
	LittleEngine::GameObject* m_pNobbin;
	LittleEngine::PhysicsComponent* m_pPhysicsNobbin;
	int m_BackgroundTouches[4];
	std::map<int, bool> m_MoveDirections;
	MoveState m_CurrentMoveState;
	//Hobbin
	LittleEngine::GameObject* m_pHobbin;
	LittleEngine::PhysicsComponent* m_pPhysicsHobbin;
	Digger* m_pDigger;

	float m_Velocity;
	float m_TimeBetweenSwitch;
	float m_CurrentSwitchTime;
	float m_SpawnXPos, m_SpawnYPos;
	int m_ChanceForSwitch;
	bool m_IsPlayerControlled;
	bool m_StopMovement;
	bool m_IsHitByBag;
	bool m_IsDisabled;

	void UpdateHobbin();
	void UpdateNobbin();

	void SetMoveState();
	void CheckForChangeMoveState(int direction);
};

