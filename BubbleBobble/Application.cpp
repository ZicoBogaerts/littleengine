#include "MiniginPCH.h"

#include "Minigin.h"
#include "MainScene.h"
#include "StartScene.h"
#include "ResourceManager.h"

#if _DEBUG
#include <vld.h>
#endif

int main(int, char* [])
{
	// tell the resource manager where he can find the game data
	LittleEngine::ResourceManager::GetInstance().Init("../Resources/");
	StartScene pStartScene{ "StartScene" };
	MainScene pMainScene{ "MainScene" };
	LittleEngine::Minigin engine{};
	engine.Run();
	return 0;
}