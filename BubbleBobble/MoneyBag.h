#pragma once

namespace LittleEngine
{
	class PhysicsComponent;
	class AnimationComponent;
	class GameObject;
}
class Gold;

class MoneyBag
{
public:
	MoneyBag(float xPos, float yPos, int nodeWidth, int nodeHeight);
	~MoneyBag();
	void AddBackgroundTouch();
	void SubstractBackgroundTouch();
	void SetBeingPushed(bool beingPushed);
	bool IsFalling() const;
	void Disable();
	void RemoveFromScene();

	MoneyBag(const MoneyBag&) = delete;
	MoneyBag(MoneyBag&&) = delete;
	MoneyBag& operator= (const MoneyBag&) = delete;
	MoneyBag& operator= (const MoneyBag&&) = delete;
private:
	enum class bagState
	{
		stuck,
		jiggling,
		gettingFree,
		falling,
		beingRemoved
	};
	bagState m_State;
	int m_BackGroundTouching;
	bool m_IsFilterSet;
	bool m_IsBeingPushed;
	float m_TimeToFallTwoStories;
	float m_TimeFalling;
	LittleEngine::AnimationComponent* m_pAnim;
	LittleEngine::PhysicsComponent* m_pPhysics;
	LittleEngine::GameObject* m_pBag;
	Gold* m_pGold;


	void Update();
};