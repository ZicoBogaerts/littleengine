#include "HighScores.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Scene.h"
#include "SceneManager.h"
#include "Text.h"
#include "Score.h"
#include <algorithm>
#include <ostream>
#include <fstream>
#include <SDL.h>

HighScores::HighScores()
	: m_NrOfHighScores{10}
	, m_pScore{nullptr}
{
	using namespace LittleEngine;
	m_HighScores.resize(m_NrOfHighScores, 0);
	m_pInitials.resize(m_NrOfHighScores);
	m_pHighScoreTexts.resize(m_NrOfHighScores);
	float xPosScores{ 220.f };
	float xPosInitials{ 80.f };
	float startYPos{ 50.f };
	float margin{ 30.f };
	m_pGameObject = new GameObject();
	for (int i{}; i < m_NrOfHighScores; i++)
	{
		float yPos = startYPos + margin * i;
		m_pHighScoreTexts[i] = new Text("0", ResourceManager::GetInstance().GetFont("Lingua.otf"), xPosScores, yPos);
		m_pHighScoreTexts[i]->SetColor({ 0,255,0,255 });
		m_pGameObject->AddComponent(m_pHighScoreTexts[i]);
		m_pInitials[i] = new Text("...", ResourceManager::GetInstance().GetFont("Lingua.otf"), xPosInitials, yPos);
		m_pInitials[i]->SetColor({ 0,255,0,255 });
		m_pGameObject->AddComponent(m_pInitials[i]);
	}
	SceneManager::GetInstance().GetActiveScene()->Add(m_pGameObject);
}

HighScores::~HighScores()
{
	std::ofstream out{"../Resources/HighScores.txt",std::ofstream::out | std::ofstream::trunc};
	for (int i{ 0 }; i < m_NrOfHighScores; i++)
	{
		out << m_HighScores[i] << ',' << m_pInitials[i]->GetText() <<  std::endl;
	}
	out.close();
}

void HighScores::AddHighScore()
{
	int score = m_pScore->GetScore();
	if (score > m_HighScores.back())
	{

		int index{ m_NrOfHighScores - 1 };
		while (m_HighScores[index] <= score && index > 0)
		{
			index--;
		}
		if (m_HighScores[0] > score)
			index++;
		std::vector<std::string> newInitials{};
		newInitials.resize(m_NrOfHighScores);
		for (int i{ index }; i < m_NrOfHighScores - 1; i++)
		{
			newInitials[i + 1] = m_pInitials[i]->GetText();
		}
		newInitials[index] = "...";
		m_pInitials[index]->SetColor({ 255,0,0,255 });
		m_pHighScoreTexts[index]->SetColor({ 255,0,0,255 });
		for (index; index < m_NrOfHighScores; index++)
		{
			m_pInitials[index]->SetText(newInitials[index]);
		}

		m_HighScores.back() = score;
		std::sort(m_HighScores.begin(), m_HighScores.end(), std::greater<int>());
		for (int i{ 0 }; i < m_NrOfHighScores; i++)
		{
			m_pHighScoreTexts[i]->SetText(std::to_string(m_HighScores[i]));
		}
		return;
	}
	for (int i{ 0 }; i < m_NrOfHighScores; i++)
	{
		m_pHighScoreTexts[i]->SetColor({ 0,255,0,255 });
		m_pInitials[i]->SetColor({ 0,255,0,255 });
	}
}

void HighScores::Enable()
{
	m_pGameObject->Enable();
}

void HighScores::Disable()
{
	m_pGameObject->Disable();
}

void HighScores::SetScoreReference(Score* pScore)
{
	m_pScore = pScore;
}

void HighScores::ReadHighScores()
{
	std::ifstream in{  "../Resources/HighScores.txt" };
	if (!in.is_open())
	{
		throw std::runtime_error("HighScores text file not found");
	}
	int index{ 0 };
	while (!in.eof())
	{
		std::string line;
		std::getline(in, line);
		if (!line.empty())
		{
			size_t it = line.find(',');
			std::string score = line.substr(0, it);
			std::string initials = line.substr(it + 1,line.length() - it);
			m_HighScores[index] = std::stoi(score);
			m_pHighScoreTexts[index]->SetText(score);
			m_pInitials[index]->SetText(initials);
			index++;
		}
	}
	in.close();
}

void HighScores::AddInitials()
{
	bool isDone{ false };
	auto it = std::find(m_HighScores.begin(), m_HighScores.end(), m_pScore->GetScore());
	if (it == m_HighScores.end())
		return;
	int index = int(it - m_HighScores.begin());
	SDL_StartTextInput();
	std::string text{};
	while (!isDone)
	{
		SDL_Event event;
		if (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDLK_KP_ENTER:
				/* Quit */
				isDone = SDL_TRUE;
				break;
			case SDL_TEXTINPUT:
				/* Add new text onto the end of our text */
				text.append(event.text.text);
				if (text.size() >= 3)
				{
					isDone = true;
					m_pInitials[index]->SetText(text.substr(0,3));
				}
				break;
			}
		}
	}
	SDL_StopTextInput();
}
