#pragma once
namespace LittleEngine
{
	class GameObject;
	class AnimationComponent;
	class PhysicsComponent;
}

class Gold
{
public:
	Gold(float xPos, float yPos, int width, int height);
	~Gold();
	void Enable();
	void Disable();
	void SetPosition(float xPos, float yPos);
	void RemoveFromScene();

	Gold(const Gold&) = delete;
	Gold(Gold&&) = delete;
	Gold& operator= (const Gold&) = delete;
	Gold& operator= (const Gold&&) = delete;
private:
	float m_LifeTime;
	float m_TimeAlive;
	bool m_PickedUp;
	LittleEngine::GameObject* m_pObject;
	LittleEngine::AnimationComponent* m_pAnim;
	LittleEngine::PhysicsComponent* m_pPhysics;
	void Update();
};

