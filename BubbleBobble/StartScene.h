#pragma once
#include "Scene.h"

class StartScene
{
public:
	StartScene(const std::string& name);
	StartScene(const StartScene&) = delete;
	StartScene(StartScene&&) = delete;
	StartScene& operator= (const StartScene&) = delete;
	StartScene& operator= (const StartScene&&) = delete;
private:
	void Initialize();
	void Update();
	void Render() const;
	void SceneActivated();
	bool m_AddInitials;
	int m_FramesWaited;

};

