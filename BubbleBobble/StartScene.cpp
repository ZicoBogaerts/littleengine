#include "StartScene.h"
#include "StartGameCommand.h"
#include "InputManager.h"
#include "Enums.h"
#include "Scene.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "RenderTexture.h"
#include "Text.h"
#include "ResourceManager.h"
#include "HighScores.h"
#include "AudioLocator.h"

StartScene::StartScene(const std::string& name)
	: m_AddInitials{false}
	, m_FramesWaited{0}
{
	using namespace LittleEngine;
	auto init = [this]() {this->Initialize(); };
	auto sceneActivated = [this]() {this->SceneActivated(); };
	auto update = [this]() {this->Update(); };
	auto render = [this]() {this->Render(); };
	Scene* pScene = new Scene{ name,init,update,render,sceneActivated };
	SceneManager::GetInstance().AddScene(pScene);
}

void StartScene::Initialize()
{
	using namespace LittleEngine;
	StartGameCommand* pStart = new StartGameCommand();
	InputManager::GetInstance().AddInputAction(InputCommand(int(inputCommands::startGame), pStart, LittleEngine::ButtonState::pressed, XINPUT_GAMEPAD_A, VK_RETURN));

	GameObject* pTitleScreen = new GameObject();
	RenderTextureComp* pTex = new RenderTextureComp("CTITLE.png",0.f,0.f,2.f);
	pTitleScreen->AddComponent(pTex);
	Text* pHighScoreText = new Text("HighScores", ResourceManager::GetInstance().GetFont("Lingua.otf"), 265.f, 5.f);
	pHighScoreText->SetColor({ 255,0,0,255 });
	pTitleScreen->AddComponent(pHighScoreText);
	SceneManager::GetInstance().GetActiveScene()->Add(pTitleScreen);

	HighScores::GetInstance().ReadHighScores();
}

void StartScene::Update()
{
	if (m_AddInitials)
	{
		m_FramesWaited++;
		if (m_FramesWaited == 2)
		{
			HighScores::GetInstance().AddInitials();
			m_AddInitials = false;
			m_FramesWaited = 0;
		}
	}
}

void StartScene::Render() const
{
}

void StartScene::SceneActivated()
{
	HighScores::GetInstance().Enable();
	m_AddInitials = true;
	LittleEngine::AudioLocator::GetAudio().PauseAllMusic();
}
