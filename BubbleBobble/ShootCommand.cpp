#include "ShootCommand.h"

ShootCommand::ShootCommand(Digger* pDigger)
	:m_pObject{pDigger}
{
}

void ShootCommand::Execute()
{
	m_pObject->SpawnProjectile();
}
