#include "MoveRightCommand.h"
#include "GameObject.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

MoveCommand::MoveCommand(ControlableObject* pObject, ControlableObject::MoveState state)
	: m_pObject{pObject}
	, m_State{state}
{
}

void MoveCommand::Execute()
{
	m_pObject->SetVelocity(m_State);
}
