#pragma once
#include "Structs.h"
#include "Observer.h"
#include <vector>

class Digger;
class Enemy;
class MainScene;

class EnemyManager : public LittleEngine::Observer
{
public:
	EnemyManager(Digger* pDigger,MainScene* pScene);
	~EnemyManager();
	void OnNotify(int event) override;
	void DiggerKilled();
	void NextLevel();
	void EnemyDied();
	void Reset();
	EnemyManager(const EnemyManager&) = delete;
	EnemyManager(EnemyManager&&) = delete;
	EnemyManager& operator= (const EnemyManager&) = delete;
	EnemyManager& operator= (const EnemyManager&&) = delete;
private:
	std::vector<Enemy*> m_pEnemies;
	LittleEngine::Float2 m_StartPos;
	int m_CurrentMaxEnemies;
	int m_MaxEnemies;
	int m_CurrentEnemiesSpawned;
	int m_EnemiesAlive;
	float m_TimeBetweenSpawns;
	float m_CurrentTimeBetweenSpawns;
	float m_TimeForRestart;
	float m_CurrentTimeRestart;
	bool m_StopSpawning;
	bool m_GoToNextLevel;
	MainScene* m_pScene;

	void Update();
	void RestartAfterDeathDigger();
};

